from datetime import datetime
import os
import json
from app.ASISTDataTools import ASISTDataTools
from app.JAGHelper import JointActivityHelper

metadataFileName = "app/static/videos/HSRData_TrialMessages_Trial-T000620_Team-TM000210_Member-na_CondBtwn-none_CondWin-na_Vers-3.metadata"

# Load the metadata messages from the metadata file
print("  metadata file: " + metadataFileName)
sorted_data = ASISTDataTools.loadAndSortMetadataFile(metadataFileName)

print("Loaded " + str(len(sorted_data)) + " messages from the metadata file.")

jah = JointActivityHelper()
for msg in sorted_data:
    jah.handle_message(msg['topic'], msg['header'], msg['msg'], msg['data'], None)

for player in jah.players().values():
    print("Player: " + player.callsign.lower())
    print("=====================================================")
    jags = player.joint_activity_model.get_known_victims()
    if jags is not None:
        for jag in jags:
            print(" - Handling a JAG: " + jag.short_string())
            # if jag.is_complete():
            #     print("    - COMPLETED JAG!!")
