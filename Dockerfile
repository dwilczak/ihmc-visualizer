# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

# For more information, please refer to https://aka.ms/vscode-docker-python-user-rights
EXPOSE 9090

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Install pip requirements
ADD requirements.txt .
RUN python -m pip install -r requirements.txt

# ADD app/templates /app/templates
# ADD app/maps /app/maps
# ADD app/__init__.py /app/__init__.py
# ADD app/ASISTDataTools.py /app/ASISTDataTools.py
# ADD app/SVGHelper.py /app/SVGHelper.py
# ADD app/SemanticMap.py /app/SemanticMap.py
# ADD app/visualizer.py /app/visualizer.py
# ADD app/TimelineHelper.py /app/TimelineHelper.py
# ADD app/MapSearchTools.py /app/MapSearchTools.py
# ADD app/JAGHelper.py /app/JAGHelper.py
# ADD app/utils /app/utils
# ADD app/models /app/models
# ADD app/handlers /app/handlers

WORKDIR /app
