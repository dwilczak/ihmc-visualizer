from app import TimelineHelper
from flask import Flask, render_template, request, make_response
from flask_socketio import SocketIO
from . import app
import json
import os
from werkzeug.utils import secure_filename
from .ASISTDataTools import ASISTDataTools
from .MapSearchTools import MapSearchTools

socketio = SocketIO(app, async_mode=None, logger=False, engineio_logger=False)

staticFolder = 'static'
if not os.path.exists(staticFolder):
    staticFolder = 'app/static'
    if not os.path.exists(staticFolder):
        print('Unable to locate an existing static folder so assuming it will be in the CWD.')
        staticFolder = 'static'

dtools = ASISTDataTools(maps_folder='maps', data_folder=os.path.join(staticFolder, 'videos'))
trialsHT = {}

@app.route("/")
def visualizer():
    return render_template("visualizer.html")

@app.route('/upload', methods=['POST'])
def upload():
    # Route to deal with the uploaded chunks
    print(request.form)
    print(request.files)

    # Remember the paramName was set to 'file', we can use that here to grab it
    file = request.files['file']

    # secure_filename makes sure the filename isn't unsafe to save
    save_path = os.path.join(staticFolder, 'videos', secure_filename(file.filename))

    open_format = 'ab' if int(request.form['dzchunkbyteoffset']) != 0 else 'wb'

    # We need to append to the file, and write as bytes
    with open(save_path, open_format) as f:
        # Goto the offset, aka after the chunks we already wrote 
        f.seek(int(request.form['dzchunkbyteoffset']))
        f.write(file.stream.read())
       
    # Giving it a 200 means it knows everything is ok
    return make_response(('Uploaded Chunk', 200))

@socketio.on('connect', namespace='/ihmc_replay_visualizer')
def sio_connect():
    print('Client connected')

@socketio.on('get_trial_list', namespace='/ihmc_replay_visualizer')
def sio_get_trial_list(msg, namespace):
    print('Getting Trial List...')
    trials = dtools.getListOfTrials()
    for trial in trials:
        trialsHT[trial['name']] = trial

    trial_list = []
    for trial in trialsHT:
        trial_list.append(trial)
    msg = {'trial_list': trial_list}
    print("Returning the list of trials:")
    print(msg)
    socketio.emit('trial_list', msg, namespace='/ihmc_replay_visualizer')

@socketio.on('delete_trial_data', namespace='/ihmc_replay_visualizer')
def sio_delete_trial_data(msg, namespace):
    if msg['trial'] in trialsHT.keys():
        trial = trialsHT[msg['trial']]
        del trialsHT[msg['trial']]

        if os.path.exists(trial['metadata-filename']):
            os.remove(trial['metadata-filename'])

        if os.path.exists(trial['video-filename']):
            os.remove(trial['video-filename'])

    sio_get_trial_list({}, '/ihmc_replay_visualizer');

@socketio.on('get_trial_data', namespace='/ihmc_replay_visualizer')
def sio_get_trial_data(msg, namespace):
    trial = trialsHT[msg['trial']]
    regenerate = msg['regenerate'] if 'regenerate' in msg.keys() else False
    print("Loading Trial Data for: " + msg['trial'])
    width, height, xtrans, ztrans, svg, timings, timelineData = dtools.loadTrial(trial, regenerateData=regenerate)

    video_url = trial['video-filename'] if 'video-filename' in trial.keys() else staticFolder + '/videos/' + msg['trial'].replace('_TrialMessages_', '_OBVideo_') + '.mp4'

    data = {
        'name': trial['info']['experiment_mission'] if 'info' in trial.keys() and trial['info'] is not None and 'experiment_mission' in trial['info'].keys() else trial['trial-name'] + ' ' + trial['map'],
        'svg': svg,
        'width': width,
        'height': height,
        'xtrans': xtrans,
        'ztrans': ztrans,
        'timings': json.loads(json.dumps(timings)), 
        'timeline': json.loads(json.dumps(timelineData)),
        'video_url': video_url
    }    
    if not os.path.exists(data['video_url']):
        data['video_url'] = staticFolder + '/videos/30minutes.mp4'

    print("  video_url: " + data['video_url'])
    socketio.emit('trial_data', data, namespace='/ihmc_replay_visualizer', room=request.sid)

@socketio.on('update_annotations', namespace='/ihmc_replay_visualizer')
def sio_update_annotations(msg, namespace):
    trial = trialsHT[msg['trial']]
    print("Saving Annotations for: " + msg['trial'])
    annotations = msg['annotations']
    updatedGroups = msg['updated']
    dtools.saveAnnotations(trial, annotations, updatedGroups)
    # print(json.dumps(msg))

@socketio.on('get_timeline_types', namespace='/ihmc_replay_visualizer')
def sio_get_timeline_types(msg, namespace):
    timelineTypes = None
    typesfile = os.path.join(staticFolder, 'TimelineCSS.json')
    if os.path.exists(typesfile):
        # load the timings
        with open(typesfile) as f:
            timelineTypes = json.load(f)
    if timelineTypes is None:
        timelineTypes = TimelineHelper.generateDefaultTimelineCSS()
    print('Returning Timeline Types...')
    print(timelineTypes)
    socketio.emit('timeline_types', timelineTypes, namespace='/ihmc_replay_visualizer')

@socketio.on('save_timeline_types', namespace='/ihmc_replay_visualizer')
def sio_save_timeline_types(msg, namespace):
    print('Saving New Timeline Types...')
    print(msg)
    if 'defaults' in msg.keys() and 'added' in msg.keys():
        typesfile = os.path.join(staticFolder, 'TimelineCSS.json')
        f = open(typesfile, "w")
        f.write(json.dumps(msg))
        f.close()

        # Now tell everyone about the changes...
        socketio.emit('timeline_types', msg, namespace='/ihmc_replay_visualizer', broadcast=True)
