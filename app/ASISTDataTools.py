from datetime import datetime
import os
import json

from scipy.sparse import base
from .SVGHelper import getBounds, BaseMap2SVG, SemanticMap2SVG, generateThreatSVG, generateMarkerBlockSVG, generatePlayerSVG, generateRubbleSVG, generateVictimSVG
from .SemanticMap import SemanticMap
from .TimelineHelper import generateDefaultTimelineCSS, generateTimelineData
# from .MapSearchTools import MapSearchTools
from .JAGHelper import JointActivityHelper

LOG_LEVEL = 1

class ASISTDataTools(object):

    LOG_LEVEL_ERROR = 0
    LOG_LEVEL_WARNING = 1
    LOG_LEVEL_INFO = 2
    LOG_LEVEL_DEBUG = 3
    LOG_LEVEL_DEBUG_2 = 4
    LOG_LEVEL_DEBUG_3 = 5

    def __init__(self, maps_folder='.', data_folder='.', log_level=LOG_LEVEL_WARNING):
        self.maps_folder = maps_folder
        self.data_folder = data_folder
        self.trial_infos = []
        LOG_LEVEL = log_level

    @staticmethod
    def set_log_level(level):
        LOG_LEVEL = level

    @staticmethod
    def log(level, msg):
        if level <= LOG_LEVEL:
            print(msg)

    @staticmethod
    def dateStringToTimestamp(dateString):
        timestamp = -1
        try:
            # timestamp = datetime.fromisoformat(dateString.replace("Z", "+00:00")).timestamp()
            # 2021-06-04T22:37:25.918301000Z
            dateTimeSplit = dateString
            if len(dateString) > 26 and dateString.endswith('000Z'):
                dateTimeSplit = dateString.split('000Z')
            elif dateString.endswith('Z'):
                dateTimeSplit = dateString.split('Z')
            if '.' in dateTimeSplit[0]:
                timestamp = datetime.strptime(dateTimeSplit[0], '%Y-%m-%dT%H:%M:%S.%f').timestamp()
            else:
                timestamp = datetime.strptime(dateTimeSplit[0], '%Y-%m-%dT%H:%M:%S').timestamp()
        except Exception as ex:
            print(ex)
            print('Invalid time: ', dateString)

        return timestamp

    @staticmethod
    def generateMsgTimestamp():
        return str(datetime.utcnow().isoformat()) + 'Z'

    def getTrialTopicMessageFromMetadata(self, filename):
        trialData = None
        with open(filename, encoding="utf8") as f:
            for line in f:
                try:
                    data = json.loads(line)
                    if data is not None and 'topic' in data.keys() and \
                    data['topic'].lower() == 'trial' and 'data' in data.keys():
                        trialData = data['data']
                        break
                except Exception as ex:
                    print(ex)
                    print('  - Invalid JSON: "', line, '"')
        return trialData

    def getHighestFileVersion(self, files, filename):
        ext = '.' + filename.split('.')[-1]
        parts = filename.split('_Vers-')
        
        highest = 0
        fn2Return = filename
        for fn in files:
            if fn.startswith(parts[0]) and fn.endswith(ext):
                try:
                    ver = int(fn.split('_Vers-')[1].split('.')[0])
                    if ver > highest:
                        highest = ver
                        fn2Return = fn
                except:
                    fn2Return = fn

        return fn2Return

    def getListOfTrials(self):
        print("Looking for data in : " + self.data_folder)
        processed = []
        self.trial_infos = []
        files = [f for f in os.listdir(self.data_folder) if os.path.isfile(os.path.join(self.data_folder,f))]
        for fn in files:
            if fn.endswith('.metadata'):
                f = self.getHighestFileVersion(files, fn)
                if f in processed:
                    continue
                processed.append(f)
                name = f.split('.metadata')[0]
                print("Processing: " + name)
                trial = {'name': name, 'metadata-filename': os.path.join(self.data_folder, f)}

                if f.lower().find('hsrdata_') != -1 or f.lower().find('pilotdata_'):
                    # Formated so see if there are other "versions" and only process the highest version number
                    parts = name.split('_')
                    print(parts)
                    trial['type'] = parts[1]
                    trial['trial-name'] = parts[2].split('-')[1]
                    trial['team'] = parts[3].split('-')[1]
                    trial['condition'] = parts[5].split('-')[1]
                    trial['map'] = parts[6].split('-')[1]
                    trial['version'] = parts[7]

                    if trial['type'] == 'TrialMessages':
                        # find video filename with the highest version number
                        htn = self.getHighestFileVersion(files, name.replace('_TrialMessages_', '_OBVideo_') + '.mp4')
                        print("   video: " + htn)
                        trial['video-filename'] = os.path.join(self.data_folder, htn)
                else:
                    trial['video-filename'] = os.path.join(self.data_folder, name + '.mp4')
                trial['video-exists'] = os.path.exists(trial['video-filename']) if 'video-filename' in trial.keys() else False
                print("   video exists: " + str(trial['video-exists']))

                # Open the file and look for a 'trial' message which can be used to set the actual trial data.
                trial['info'] = self.getTrialTopicMessageFromMetadata(trial['metadata-filename'])

                if trial['info'] is not None:
                    trial['type'] = 'TrialMessages'
                    if 'experiment_name' in trial['info'].keys():
                        trial['trial-name'] = trial['info']['experiment_name']
                    if 'experiment_mission' in trial['info'].keys():
                        trial['team'] = trial['info']['experiment_mission']
                    if 'group_number' in trial['info'].keys():
                        trial['condition'] = trial['info']['group_number']
                    if 'experiment_mission' in trial['info'].keys():
                        trial['map'] = trial['info']['experiment_mission']

                    if 'map_name' not in trial['info']:
                        if trial['name'].lower().find("training") != -1:
                            trial['info']['map_name'] = 'Saturn_Training_1.4_3D'
                        elif trial['name'].lower().find("competency") != -1:
                            trial['info']['map_name'] = 'Saturn_Competency_Test_1.4_3D'
                        else:
                            trial['info']['map_name'] = 'Saturn_1.4_3D'

                if trial['info'] is not None and 'date' in trial['info'].keys():
                    trial['ts'] = self.dateStringToTimestamp(trial['info']['date'])
                else:
                    trial['ts'] = self.dateStringToTimestamp(self.generateMsgTimestamp())

                self.trial_infos.append(trial)

        self.trial_infos = sorted(self.trial_infos, key=lambda d: d['ts'], reverse=True)
        return self.trial_infos


    @staticmethod
    def genVictimInfo(id, type, x, z, start = 0, end = 9999999, triaged = False, unlocked = True, evacuated = False):
        victim = {'id': str(id), 
                  'start': start,
                  'end': end,
                  'is-critical': False, 
                  'triaged': triaged,
                  'evacuated': evacuated,
                  'unlocked': unlocked,
                  'x': x,
                  'z': z}
        if type == 'block_victim_proximity' or type == 'CRITICAL' or type == 'victim_saved_c' or type == 'victim_c':
            victim['is-critical'] = True
            victim['id'] += ' C'
        elif type == 'block_victim_1b' or type == 'victim_saved_b' or type == 'victim_b':
            victim['id'] += ' B'
        else:
            victim['id'] += ' A'

        return victim

    @staticmethod
    def getPlayerCallsignFromNameOrId(playername, participant_id, client_info):
        callsign = None
        for client in client_info:
            if 'participant_id' in client.keys():
                if participant_id is not None and client['participant_id'] == participant_id:
                    callsign = client['callsign'].lower().strip()
            else:
                if participant_id is not None and client['participantid'] == participant_id:
                    callsign = client['callsign'].lower().strip()
            if callsign is None and playername is not None and 'playername' in client.keys() and client['playername'] == playername:
                callsign = client['callsign'].lower().strip()
        callsign = ASISTDataTools.getCallsignFromMetadataCallsign(callsign)
        if callsign is None:
            return playername
        return callsign

    @staticmethod
    def getCallsignFromMetadataCallsign(callsign):
        if callsign is not None:
            if callsign.lower() == 'red' or callsign.lower() == 'alpha':
                return 'Red'
            if callsign.lower() == 'green' or callsign.lower() == 'beta':
                return 'Green'
            if callsign.lower() == 'blue' or callsign.lower() == 'delta':
                return 'Blue'
            if len(callsign) > 0:
                return callsign
        return None


    @staticmethod
    def getPlayerCallsign(data, client_info):
        playername = data['playername'] if 'playername' in data.keys() else None
        participant_id = data['participant_id'] if 'participant_id' in data.keys() else None
        return ASISTDataTools.getPlayerCallsignFromNameOrId(playername, participant_id, client_info)

    @staticmethod
    def getPlayerColorFromCallsign(callsign):
        if callsign is not None:
            if callsign == 'Red':
                return 'rgba(255, 0, 0, 0.6)'
            if callsign == 'Green':
                return 'rgba(0, 255, 0, 0.6)'
            if callsign == 'Blue':
                return 'rgba(0, 0, 255, 0.6)'
        return 'rgba(20, 20, 20, 0.6)'

    @staticmethod
    def getPlayerColor(data, client_info):
        return ASISTDataTools.getPlayerColorFromCallsign(ASISTDataTools.getPlayerCallsign(data, client_info))

    @staticmethod
    def updateBasemap(basemap, x, y, z):
        if basemap is None:
            return basemap

        # remove any block which are at location x,z
        newData = []
        if 'data' in basemap.keys():
            for block in basemap['data']:
                # if block[0][0] == x and block[0][1] == y and block[0][2] == z:
                if block[0][0] == x and block[0][2] == z:
                    continue
                newData.append(block)
            basemap['data'] = newData

        return basemap


    def saveAnnotations(self, trial_info, annotations, updatedGroups):
        if trial_info is None:
            return

        timelineData = {}
        if os.path.exists(trial_info['metadata-filename'] + '.timeline'):
            # load the timings
            with open(trial_info['metadata-filename'] + '.timeline') as f:
                timelineData = json.load(f)

        if 'Annotations' not in timelineData.keys():
            timelineData['Annotations'] = {}
        if isinstance(timelineData['Annotations'], list):
            timelineData['Annotations'] = {'Teamwork': timelineData['Annotations']}

        for group in updatedGroups:
            if group in annotations.keys():
                timelineData['Annotations'][group] = annotations[group]
            elif group in timelineData['Annotations'].keys():
                # if the group is updated but not in annotations, then delete it.
                del timelineData['Annotations'][group]

        f = open(trial_info['metadata-filename'] + ".timeline", "w")
        f.write(json.dumps(timelineData))
        f.close()

        # print(json.dumps(timelineData, indent=4))


    @staticmethod
    def loadAndSortMetadataFile(filename):
        file_data = []
        if os.path.exists(filename):
            with open(filename, encoding="utf8") as f:
                for line in f:
                    data = json.loads(line)
                    if 'error' in data.keys() and 'data' in data['error'].keys():
                        data['data'] = json.loads(data['error']['data'])
                    if "@timestamp" in data.keys() and 'topic' in data.keys() and 'data' in data.keys():
                        timestamp = ASISTDataTools.dateStringToTimestamp(data["@timestamp"])
                        data['ts'] = timestamp
                        file_data.append(data)
            file_data = sorted(file_data, key=lambda d: d['ts'])
        return file_data


    def loadTrial(self, trial_info, generateSVG=True, generateHypothesisData=False, regenerateData=False):
        if trial_info is None:
            return 100, 100, 0, 0, '', [], []
        if 'info' not in trial_info.keys() or trial_info['info'] is None:
            return 100, 100, 0, 0, '', [], []
        if 'map_name' not in trial_info['info'].keys():
            return 100, 100, 0, 0, '', [], []

        width = -1
        height = -1
        xtrans = -1
        ztrans = -1
        svg_map = ''
        timings = []
        timelineData = {}
        expType = 2
        rubble_index = 0
        urns = []
        sub_types = []

        hypoDataOutput = 1000
        planning_stop_time = 0

        print("Loading Trail: " + trial_info['name'])

        files = [f for f in os.listdir(self.data_folder) if os.path.isfile(os.path.join(self.data_folder,f))]

        # Load the Semantic Map from the Semantic map file
        semanticmap = None
        filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_sm_v1.0.json')
        if not os.path.exists(filename):
            filename = os.path.join(self.maps_folder, 'Saturn_2.1_3D_sm_v1.0.json')
        if os.path.exists(filename):
            print("  - semantic map: " + filename)
            semanticmap = SemanticMap()
            semanticmap.load_semantic_map(filename)

        if semanticmap is None:
            print ("   *** No Semantic map at: " + filename)
            return 100, 100, 0, 0, '', [], []

        # trial_info['mapSearchTools'] = MapSearchTools()
        # trial_info['mapSearchTools'].setSemanticMap(semanticmap)

        timelineFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.timeline')
        if os.path.exists(timelineFilename):
            # load the timeline items
            with open(timelineFilename) as f:
                timelineData = json.load(f)

        # maptoolsFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.maptools')
        # if not regenerateData and os.path.exists(maptoolsFilename):
        #     trial_info['mapSearchTools'].load(maptoolsFilename)

        # See if we have already generated this data and saved it.
        timingsFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.timings')
        if not regenerateData and os.path.exists(timingsFilename):
            print("  - Loading Previously saved Data")
            # load the timings
            with open(timingsFilename) as f:
                timings = json.load(f)

            # load the svg
            svglines = []
            svgFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.svg')
            with open(svgFilename) as f:
                for line in f:
                    svglines.append(line)

            # parse the svg rect on the 3rd line and get the width and height
            # '<rect x="0" y="0" width="' + str(width) + '" height="' + str(height) + '" style="fill:rgb(255,255,255)"></rect>\n'
            # sizeInfo = json.loads(svglines[2].replace('<rect x="0" y="0" width=', '{"w":').replace(' height=', ', "h":').replace(' style="fill:rgb(255,255,255)"></rect>', '}'))
            sizeInfo = json.loads(
            svglines[2].replace('<rect id="x_', '{"x":"').replace('_z_', '", "z":"').replace(' x="0" y="0" width=', ', "w":').replace(' height=', ', "h":').replace(' style="fill:rgb(255,255,255)"></rect>', '}'))
            xtrans = int(sizeInfo['x'])
            ztrans = int(sizeInfo['z'])
            width = int(sizeInfo['w'])
            height = int(sizeInfo['h'])

            # strip out the svg_map lines from the svg loaded
            for i in range(3, len(svglines)-1):
                svg_map += svglines[i] + '\n'

        if regenerateData or len(timings) <= 0:
            print("  - Generating new Data")
            svg_map = ''
            timings = []

            jah = JointActivityHelper()

            roleMap = {
                'NONE': {'name': 'None', 'max_level':0.0},
                'Hazardous_Material_Specialist': {'name': 'Rubbler', 'max_level': 40.0},
                'Search_Specialist': {'name': 'Searcher', 'max_level': 20.0},
                'Medical_Specialist': {'name': 'Medic', 'max_level': 30.0},
                'Engineering_Specialist': {'name': 'Rubbler', 'max_level': 100.0},
                'Transport_Specialist': {'name': 'Transporter', 'max_level': 100.0}
            }

            # generate SVG for the Semantic Map
            bounds = getBounds(semanticmap.semantic_map)
            xtrans = bounds[0]-2
            ztrans = bounds[1]-2
            width = bounds[2]+4
            height = bounds[3]+4
            rotate = 0
            trans_x = 0
            trans_y = 0
            if generateSVG:
                semanticMapSVG = SemanticMap2SVG(semanticmap.semantic_map, xtrans, ztrans)

            # Load the Base Map from the base map file
            basemap = None
            filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_bm_v1.0.json')
            if not os.path.exists(filename):
                filename = os.path.join(self.maps_folder, 'Saturn_2.1_3D_bm_v1.0.json')
            if os.path.exists(filename):
                print("       base map: " + filename)
                # generate SVG for the base map
                with open(filename) as json_file:
                    basemap = json.load(json_file)

            missionStartTime = -1
            mbidx = 0
            thidx = 0
            vicidx = 0
            step = 1

            teamScore = 0
            victims = {}
            rubble = {}
            markers = {}
            threats = {}
            players = {}
            playerSVG = []
            pathSVG = []
            twentyMinutesAsMS = 18*60*1000
            timeline = {
                'score': [{'st': 0, 'score': 0}],
                'endTime': -1,
                'unlocked': []      # {'vid': '', 'st': -1}]
            }
            
            # Load the metadata messages from the metadata file
            if os.path.exists(trial_info['metadata-filename']):
                print("  metadata file: " + trial_info['metadata-filename'])
                sorted_data = ASISTDataTools.loadAndSortMetadataFile(trial_info['metadata-filename'])

                if 'intervention_agents' in trial_info['info'].keys():
                    expType = 3

                timings.append(['mission_score', 0, -1, 'st', 0])
                for client in trial_info['info']['client_info']:
                    callsign = self.getCallsignFromMetadataCallsign(client['callsign'])
                    if callsign is None:
                        continue
                    players[callsign] = {"callsign": callsign,
                                        "participant_id": client['participant_id'],
                                        "markerblocklegend": client['markerblocklegend'],
                                        "staticmapversion": client['staticmapversion'],
                                        "last_role": 'NONE',
                                        "last_tool_level": 0,
                                        "last_location": 'UNKNOWN',
                                        "last_state": '',
                                        "locs": [],
                                        "step": step,
                                        "last_x": -9999,
                                        "last_z": -9999,
                                        "last_yaw": -9999,
                                        "color": self.getPlayerColorFromCallsign(callsign),
                                        "debris_cleared": 0,
                                        "victims_moved": 0,
                                        "critical_evacuated": 0,
                                        "non-critical_evacuated": 0,
                                        "critical_triaged": 0,
                                        "non-critical_triaged": 0,
                                        "asr_ts": -1,
                                        "id": len(players)+1,
                                        "carrying": [], # [{'vid': '', 'st': -1, 'et': -1}]
                                        "triaging": [], # [{'vid': '', 'type': '', 'st': -1, 'et': -1}]
                                        "rubble": [],   # [{'rid': '', 'st': -1, 'x': -1, 'y': -1}]
                                        "frozen": [],
                                        "tripped_hazard": [],
                                        "role_changes": [{'role': 'unknown', 'level': 0.0, 'st':0, 'et': twentyMinutesAsMS}],
                                        "jag": {}
                                        }
                    timings.append(['m_' + callsign[0] + '_st', 0, -1, 'st', players[callsign]['last_state']])
                    timings.append(['m_' + callsign[0] + '_sc', 0, -1, 'st', 0])
                    timings.append(['m_' + callsign[0] + '_rl', 0, -1, 'st', players[callsign]['last_role']])
                    if expType < 3:
                        timings.append(['m_' + callsign[0] + '_tl', 0, -1, 'st', players[callsign]['last_tool_level']])
                    timings.append(['m_' + callsign[0] + '_rb', 0, -1, 'st', players[callsign]['debris_cleared']])
                    timings.append(['m_' + callsign[0] + '_mv', 0, -1, 'st', players[callsign]['victims_moved']])
                    timings.append(['m_' + callsign[0] + '_nv', 0, -1, 'st', players[callsign]['non-critical_triaged']])
                    timings.append(['m_' + callsign[0] + '_cv', 0, -1, 'st', players[callsign]['critical_triaged']])
                    timings.append(['m_' + callsign[0] + '_loc', 0, -1, 'st', players[callsign]['last_location']])

                last_elapsed_time = -1
                finished = False
                for msg in sorted_data:
                    topic = msg['topic']
                    data = msg['data']

                    jah.handle_message(topic, msg['header'], msg['msg'], data, None)

                    if topic == 'observations/events/mission':
                        if data['mission_state'] == 'Stop':
                            finished = True

                            # Mark an end of the mission here
                            timeline['endTime'] = last_elapsed_time
                            
                            break
                        elif data['mission_state'] == 'Start':
                            missionStartTime = self.dateStringToTimestamp(msg['msg']['timestamp'])
                            rubble = {}
                            victims = {}
                            threats = {}
                    elif topic == 'observations/events/mission/planning':
                        if 'state' in data and data['state'].lower() == 'stop':
                            if 'elapsed_milliseconds' in data:
                                planning_stop_time = data['elapsed_milliseconds']
                            else:
                                planning_stop_time = 123000
                    elif topic == 'observations/state':
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            ts = data['elapsed_milliseconds']
                            if ts < 0:
                                if missionStartTime >= 0:
                                    ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                                else:
                                    continue
                            last_elapsed_time = ts

                            x = round(data['x'], 1)
                            z = round(data['z'], 1)
                            yaw = data['yaw']
                        
                            if player['last_x'] != x or player['last_z'] != z or player['last_yaw'] != yaw:
                                player['step'] += 1
                                if player['step'] > step:
                                    location = semanticmap.get_locations_containing(x, z, use_updated_map=False)
                                    location = location[0]['name'] if len(location) > 0 else "UNKNOWN"
                                    player['locs'].append({
                                        'ts': ts,
                                        'state': player['last_state'],
                                        'x': x,
                                        'z': z,
                                        'yaw': yaw,
                                        'role': player['last_role']
                                    })
                                    player['step'] = 0
                                    player['last_x'] = x
                                    player['last_z'] = z
                                    player['last_yaw'] = yaw
                                    if location != 'UNKNOWN' and location != player['last_location']:
                                        player['last_location'] = location
                                        if len(location) > 30:
                                            location = location[0:29]
                                        timings.append(['m_' + callsign[0] + '_loc', ts, -1, 'st', location])
                    elif topic == 'observations/events/player/role_selected':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts

                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            if ts >= 0:
                                player['locs'].append({
                                    'ts': ts,
                                    'state': player['last_state'],
                                    'x': player['last_x'],
                                    'z': player['last_z'],
                                    'yaw': player['last_yaw'],
                                    'role': data['new_role']
                                })
                            else:
                                ts = 0
                            player['last_role'] = data['new_role']
                            player['last_tool_level'] = 100
                            timings.append(['m_' + callsign[0] + '_rl', ts, -1, 'st', roleMap[player['last_role']]['name']])
                            if expType < 3:
                                timings.append(['m_' + callsign[0] + '_tl', ts, -1, 'st', player['last_tool_level']])

                            player['role_changes'][-1]['et'] = ts
                            player['role_changes'].append({'role': roleMap[player['last_role']]['name'], 'level': 100.0, 'st': ts, 'et': twentyMinutesAsMS})

                    elif topic == 'observations/events/player/tool_used':
                        ts = data['elapsed_milliseconds']
                        if ts < 0:
                            if missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            else:
                                continue
                        last_elapsed_time = ts

                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            max_level = roleMap[player['last_role']]['max_level']
                            if data['tool_type'] == 'STRETCHER':
                                continue
                            if data['tool_type'] == 'STRETCHER_OCCUPIED':
                                # Stretcher durability is off by 1 and is only valid on 'Stretcher_Occupied' and not on 'Strecher' tool type values.
                                data['durability'] = data['durability'] + 1
                            else:
                                # Hammer and Med Kit messages are avg 900ms late.
                                ts += 900
                            # print(data)
                            player['last_tool_level'] = round(100.0 * data['durability'] / max_level, 1)
                            player['locs'].append({
                                'ts': ts,
                                'state': player['last_state'],
                                'x': player['last_x'],
                                'z': player['last_z'],
                                'yaw': player['last_yaw'],
                                'role': player['last_role']
                            })
                            if expType < 3:
                                timings.append(['m_' + callsign[0] + '_tl', ts, -1, 'st', player['last_tool_level']])

                            if player['role_changes'][-1]['level'] != player['last_tool_level']:
                                player['role_changes'][-1]['et'] = ts
                                player['role_changes'].append({'role': roleMap[player['last_role']]['name'], 'level': player['last_tool_level'], 'st': ts, 'et': twentyMinutesAsMS})
                        # print ('player: ' + callsign + '  role: ' + player['last_role'] + ' tool: ' + str(player['last_tool_level']))

                    elif topic == 'ground_truth/mission/victims_list':
                        if 'mission_victim_list' in data.keys():
                            for victim in data['mission_victim_list']:
                                vic = self.genVictimInfo(victim['unique_id'], victim['block_type'], victim['x'], victim['z'])
                                if vic['is-critical']:
                                    vic['unlocked'] = False
                                vic['sid'] = 'v_' + str(vicidx)
                                vicidx = vicidx + 1
                                victims[vic['id']] = [vic]
                                basemap = self.updateBasemap(basemap, victim['x'], victim['y'], victim['z'])
                    elif topic == 'observations/events/player/victim_evacuated' or \
                         topic == 'observations/events/server/victim_evacuated':

                        if 'success' in data.keys() and data['success']:
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            # print(str(data['elapsed_milliseconds']) + ',' + str(data['victim_id']) + ',' + data['triage_state'] + ',' + data['playername'])
                            player = {"locs": []}
                            if callsign is not None and callsign in players.keys():
                                player = players[callsign]

                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts, triaged=True, evacuated=True)
                            vic['sid'] = 'v_' + str(vicidx)
                            vicidx = vicidx + 1
                            if vic['id'] not in victims.keys():
                                victims[vic['id']] = [vic]
                            else:
                                if victims[vic['id']][-1]['end'] == 999999:
                                    victims[vic['id']][-1]['end'] = ts 
                                victims[vic['id']].append(vic)
                            if vic['is-critical']:
                                player['critical_evacuated'] += 1
                                timings.append(['m_' + callsign[0] + '_ce', ts, -1, 'st', player['critical_evacuated']])
                            else:
                                player['non-critical_evacuated'] += 1
                                timings.append(['m_' + callsign[0] + '_ne', ts, -1, 'st', player['non-critical_evacuated']])

                    elif topic == 'observations/events/player/triage':
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        # print(str(data['elapsed_milliseconds']) + ',' + str(data['victim_id']) + ',' + data['triage_state'] + ',' + data['playername'])
                        player = {"locs": []}
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]

                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts, triaged=True)
                        if data['triage_state'] == 'SUCCESSFUL':
                            player['last_state'] = ''
                            vic['sid'] = 'v_' + str(vicidx)
                            vicidx = vicidx + 1
                            if vic['id'] not in victims.keys():
                                victims[vic['id']] = [vic]
                            else:
                                victims[vic['id']][-1]['end'] = ts
                                victims[vic['id']].append(vic)
                            if vic['is-critical']:
                                player['critical_triaged'] += 1
                                timings.append(['m_' + callsign[0] + '_cv', ts, -1, 'st', player['critical_triaged']])
                            else:
                                player['non-critical_triaged'] += 1
                                timings.append(['m_' + callsign[0] + '_nv', ts, -1, 'st', player['non-critical_triaged']])

                            # mark the end of a successful timeline triage
                            # Need to end any other players triaging of this same victim.
                            for key in players:
                                otherPlayer = players[key]
                                if len(otherPlayer['triaging']) > 0 and otherPlayer['triaging'][-1]['vid'] == vic['id']:
                                    otherPlayer['triaging'][-1]['et'] = ts
                                    if key == callsign:
                                        player['triaging'][-1]['triaged'] = True
                                    else:
                                        otherPlayer['last_state'] = ''
                                        timings.append(['m_' + otherPlayer['callsign'][0] + '_st', ts, -1, 'st', otherPlayer['last_state']])

                            # if there was never a record of this triage starting, update it here.
                            if len(player['triaging']) <= 0 or player['triaging'][-1]['vid'] != vic['id']:
                                st = ts - 3000 if expType > 2 else ts - 15000 if vic['is-critical'] else ts - 7500
                                player['triaging'].append({'vid': vic['id'], 'is-critical': vic['is-critical'], 'st': st, 'et': ts, 'triaged': True})
                                timings.append(['m_' + callsign[0] + '_st', st, -1, 'st', 'triaging'])

                        elif data['triage_state'] == 'IN_PROGRESS':
                            player['last_state'] = 'triaging'

                            et = ts + 3000 if expType > 2 else ts + 15000 if vic['is-critical'] else ts + 7500

                            # mark the start of a timeline triage
                            player['triaging'].append({'vid': vic['id'], 'is-critical': vic['is-critical'], 'st': ts, 'et': et, 'triaged': False})

                        elif data['triage_state'] == 'UNSUCCESSFUL':
                            player['last_state'] = ''

                            # mark the end of an unsuccessful timeline triage
                            player['triaging'][-1]['et'] = ts

                        timings.append(['m_' + callsign[0] + '_st', ts, -1, 'st', player['last_state']])

                        player['locs'].append({
                            'ts': ts,
                            'state': player['last_state'],
                            'x': player['last_x'],
                            'z': player['last_z'],
                            'yaw': player['last_yaw'],
                            'role': player['last_role']
                        })

                    elif topic == 'observations/events/player/victim_picked_up':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts)
                        if vic['id'] not in victims.keys():
                            victims[vic['id']] = [vic]
                        else:
                            victims[vic['id']][-1]['end'] = ts
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            players[callsign]['victims_moved'] += 1
                            players[callsign]['last_state'] = 'carrying'
                            timings.append(['m_' + callsign[0] + '_mv', ts, -1, 'st', players[callsign]['victims_moved']])
                            timings.append(['m_' + callsign[0] + '_st', ts, -1, 'st', players[callsign]['last_state']])

                            # mark the start of a victim carry
                            players[callsign]['carrying'].append({'vid': vic['id'], 'st': ts, 'et': twentyMinutesAsMS, 'is-critical': vic['is-critical']})

                    elif topic == 'observations/events/player/victim_placed':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts)
                        vic['sid'] = 'v_' + str(vicidx)
                        vicidx = vicidx + 1
                        if vic['id'] not in victims.keys():
                            victims[vic['id']] = [vic]
                        else:
                            if victims[vic['id']][-1]['end'] == 999999:
                                victims[vic['id']][-1]['end'] = ts 
                            vic['triaged'] = victims[vic['id']][-1]['triaged']
                            vic['unlocked'] = victims[vic['id']][-1]['unlocked']
                            vic['evacuated'] = victims[vic['id']][-1]['evacuated']
                            victims[vic['id']].append(vic)
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            players[callsign]['last_state'] = ''
                            timings.append(['m_' + callsign[0] + '_st', ts, -1, 'st', players[callsign]['last_state']])

                            # mark the end of a victim carry
                            if len(players[callsign]['carrying']) > 0:
                                players[callsign]['carrying'][-1]['et'] = ts

                    elif topic == 'observations/events/player/proximity_victim' or \
                         topic == 'observations/events/player/proximity_block':
                        if data['victim_id'] != -1 and ((expType == 2 and data['players_in_range'] >= 3) or \
                            (expType == 3 and ('awake' in data and data['awake']) or ('awake' not in data and data['players_in_range'] >= 2))):
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            vic = self.genVictimInfo(data['victim_id'], 'CRITICAL', data['victim_x'], data['victim_z'], start=ts)
                            vic['sid'] = 'v_' + str(vicidx)
                            vicidx = vicidx + 1
                            if vic['id'] not in victims.keys():
                                victims[vic['id']] = [vic]
                            else:
                                victims[vic['id']][-1]['end'] = ts
                                victims[vic['id']].append(vic)

                            # mark the time a Critical victim is unlocked
                            timeline['unlocked'].append({'vid': vic['id'], 'st': ts})

                    elif topic == 'ground_truth/mission/blockages_list':
                        if 'mission_blockage_list' in data.keys():
                            for blockage in data['mission_blockage_list']:
                                id = str(blockage['x']) + '_' + str(blockage['z'])
                                if id not in rubble.keys():
                                    rubble[id] = [{'id': 'rb' + str(rubble_index),
                                                'start': 0,
                                                'end': 9999999,
                                                'x': blockage['x'],
                                                'z': blockage['z'],
                                                'total': 0}]
                                    rubble_index = rubble_index + 1
                                    basemap = self.updateBasemap(basemap, blockage['x'], blockage['y'], blockage['z'])
                                rubble[id][0]['total'] += 1
                    elif topic == 'observations/events/mission/perturbation':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if 'perturbation' not in timeline:
                            timeline['perturbation'] = []
                        if 'type' in data:
                            if data['type'] == 'blackout':
                                if data['mission_state'].lower() == 'start':
                                    timeline['perturbation'].append(
                                        {'start':ts, 'end': 9999999, 'type': 'blackout'}
                                    )
                                elif len(timeline['perturbation']) > 0:
                                    timeline['perturbation'][-1]['end'] = ts
                            elif data['type'] == 'rubble':
                                if data['mission_state'].lower() == 'start':
                                    timeline['perturbation'].append(
                                        {'start':ts, 'end': 9999999, 'type': 'rubble'}
                                    )

                    elif topic == 'observations/events/mission/perturbation_rubble_locations':
                        if 'elapsed_milliseconds' in data:
                            ts = data['elapsed_milliseconds']
                            last_elapsed_time = ts
                        elif ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                        if 'mission_blockage_list' in data.keys():
                            # TODO:: possible issue if marker block or victim has been placed at any of these locations.
                            for blockage in data['mission_blockage_list']:
                                id = str(blockage['x']) + '_' + str(blockage['z'])
                                if id not in rubble.keys():
                                    rubble[id] = [{'id': 'rb' + str(rubble_index),
                                                'start': ts,
                                                'end': 9999999,
                                                'x': blockage['x'],
                                                'z': blockage['z'],
                                                'total': 0}]
                                    rubble_index = rubble_index + 1
                                    basemap = self.updateBasemap(basemap, blockage['x'], blockage['y'], blockage['z'])
                                if rubble[id][0]['total'] < 3:
                                    rubble[id][0]['total'] += 1
                    elif topic == 'observations/events/player/rubble_collapse' or \
                         topic == 'observations/events/server/rubble_collapse':
                        # TODO:: possible issue if marker block or victim has been placed at any of these locations.
                        # print("Handling topic: " + topic)
                        # print(json.dumps(data))

                        try:
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts

                            # update the threat trigger
                            id = str(data['triggerLocation_x']) + '_' + str(data['triggerLocation_z'])
                            if id in threats.keys():
                                threats[id][-1]['end'] = ts
                                threats[id].append({
                                    'id': 'th_' + str(thidx),
                                    'start': ts,
                                    'end': ts+40000,
                                    'x': data['triggerLocation_x'],
                                    'z': data['triggerLocation_z'],
                                    'state': 'activated'
                                })
                                thidx = thidx + 1
                                threats[id].append({
                                    'id': 'th_' + str(thidx),
                                    'start': ts+40000,
                                    'end': 9999999,
                                    'x': data['triggerLocation_x'],
                                    'z': data['triggerLocation_z'],
                                    'state': 'armed'
                                })
                                thidx = thidx + 1

                            # update the rubble which collapsed.
                            fromX = data['fromBlock_x']
                            toX = data['toBlock_x'] + 1
                            fromZ = data['fromBlock_z']
                            toZ = data['toBlock_z'] + 1
                            fromY = data['fromBlock_y']
                            toY = data['toBlock_y'] + 1
                            maxTotal = toY - fromY
                            if maxTotal > 3:
                                maxTotal = 3
                            # print("  maxTotal = " + str(maxTotal) + " fromY:" + str(fromY) + " toY:" + str(toY))
                            handledIDs = []
                            for x in range(fromX, toX):
                                for z in range(fromZ, toZ):
                                    id = str(x) + '_' + str(z)
                                    if id in rubble.keys():
                                        if rubble[id][-1]['end'] == 9999999 and rubble[id][-1]['total'] >= maxTotal:
                                            continue
                                        if rubble[id][-1]['end'] == 9999999:
                                            rubble[id][-1]['end'] = ts
                                    else:
                                        rubble[id] = []
                                        for y in range(fromY, toY):
                                            basemap = self.updateBasemap(basemap, x, y, z)

                                    rubble[id].append({
                                        'id': 'rb' + str(rubble_index),  #rubble[id][0]['id'],
                                        'start': ts,
                                        'end': 9999999,
                                        'x': x,
                                        'z': z,
                                        'total': maxTotal
                                    })
                                    rubble_index = rubble_index + 1
                                    # print("Rubble_Collapse at [" + str(ts) + "] added rubble at " + id + "  total:" + str(rubble[id][-1]['total']))
                        except:
                            print("Invalid rubble_collapse data!!")
                    elif topic == 'observations/events/player/rubble_destroyed':
                        id = str(data['rubble_x']) + '_' + str(data['rubble_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts

                        if id in rubble.keys():
                            if rubble[id][-1]['end'] == 9999999:
                                rubble[id][-1]['end'] = ts
                            total = rubble[id][-1]['total'] - 1
                            if total > 0:
                                rubble[id].append({
                                    'id': 'rb' + str(rubble_index),  #rubble[id][0]['id'],
                                    'start': ts,
                                    'end': 9999999,
                                    'x': data['rubble_x'],
                                    'z': data['rubble_z'],
                                    'total': total
                                })
                                rubble_index = rubble_index + 1
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            if callsign is not None and callsign in players.keys():
                                players[callsign]['debris_cleared'] += 1
                                timings.append(['m_' + callsign[0] + '_rb', ts, -1, 'st', players[callsign]['debris_cleared']])

                                # mark the time rubble is destroyed
                                players[callsign]['rubble'].append({'rid': id, 'st': ts})

                    elif topic == 'ground_truth/mission/freezeblock_list':
                        if 'mission_freezeblock_list' in data.keys():
                            for block in data['mission_freezeblock_list']:
                                id = str(block['x']) + '_' + str(block['z'])
                                threats[id] = [{'id': 'th_' + str(thidx),
                                            'start': 0,
                                            'end': 9999999,
                                            'x': block['x'],
                                            'z': block['z'],
                                            'state': 'armed'}]
                                thidx = thidx + 1
                    elif topic == 'observations/events/player/freeze':
                        id = str(data['player_x']) + '_' + str(data['player_z'])
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            if data['state_changed_to'] == 'FROZEN':
                                player['last_state'] = 'frozen'
                                if id in threats.keys():
                                    threats[id][-1]['end'] = ts
                                    threats[id].append({
                                        'id': 'th_' + str(thidx),
                                        'start': ts,
                                        'end': 9999999,
                                        'x': data['player_x'],
                                        'z': data['player_z'],
                                        'state': 'activated'
                                    })
                                    thidx = thidx + 1
                                # mark when a player is fozen
                                player['frozen'].append({'st': ts, 'et': twentyMinutesAsMS})
                            else:
                                player['last_state'] = ''
                                if id in threats.keys():
                                    threats[id][-1]['end'] = ts
                                    threats[id].append({
                                        'id': 'th_' + str(thidx),
                                        'start': ts,
                                        'end': 9999999,
                                        'x': data['player_x'],
                                        'z': data['player_z'],
                                        'state': 'disarmed'
                                    })
                                    thidx = thidx + 1
                                # mark when a player is unfozen
                                player['frozen'][-1]['et'] = ts

                            timings.append(['m_' + callsign[0] + '_st', ts, -1, 'st', player['last_state']])

                        player['locs'].append({
                            'ts': ts,
                            'state': player['last_state'],
                            'x': player['last_x'],
                            'z': player['last_z'],
                            'yaw': player['last_yaw'],
                            'role': player['last_role']
                        })

                    elif topic == 'ground_truth/mission/threatsign_list':
                        if 'mission_threatsign_list' in data.keys():
                            for block in data['mission_threatsign_list']:
                                id = str(block['x']) + '_' + str(block['z'])
                                threat_type = 'armed'
                                if expType == 2:
                                    threat_type = 'warning_sign'
                                threats[id] = [{'id': 'th_' + str(thidx),
                                            'start': 0,
                                            'end': 9999999,
                                            'x': block['x'],
                                            'z': block['z'],
                                            'state': threat_type}]
                                thidx = thidx + 1

                    elif topic == 'observations/events/player/marker_placed':
                        id = str(data['marker_x']) + '_' + str(data['marker_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if id not in markers.keys():
                            markers[id] = []
                        elif markers[id][-1]['end'] == 9999999:
                            markers[id][-1]['end'] = ts
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])

                        #Need to update to handle new marker blocks
                        block_type = data['type']
                        type = -1
                        if expType == 2:
                            type = data['type'][-1]
                            if type == 'Marker Block 1' or type == 'Marker Block 2' or type == 'Marker Block 3':
                                type = int(data['type'][-1])
                            else:
                                type = 1
                        markers[id].append({
                            'id': 'm'+str(mbidx),
                            'start': ts,
                            'end': 9999999,
                            'x': data['marker_x'],
                            'z': data['marker_z'],
                            'number': type,
                            'block_type': block_type,
                            'callsign': callsign,
                            'color': self.getPlayerColorFromCallsign(callsign)
                        })
                        mbidx = mbidx + 1
                    elif topic == 'observations/events/player/marker_removed':
                        id = str(data['marker_x']) + '_' + str(data['marker_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if id in markers.keys() and markers[id][-1]['end'] == 9999999:
                            markers[id][-1]['end'] = ts

                    elif topic == 'observations/events/scoreboard':
                        if 'scoreboard' in data.keys() and 'TeamScore' in data['scoreboard'].keys():
                            teamScore = data['scoreboard']['TeamScore']
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['msg']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            for playername in data['scoreboard']:
                                score = data['scoreboard'][playername]
                                callsign = self.getPlayerCallsignFromNameOrId(playername, playername, trial_info['info']['client_info'])
                                if callsign is not None and callsign in players.keys():
                                    player = players[callsign]
                                    timings.append(['mission_score', ts, -1, 'st', teamScore])
                                    timings.append(['m_' + callsign[0] + '_sc', ts, -1, 'st', score])

                                    #mark when the score is updated.
                                    timeline['score'].append({'score': teamScore, 'st': ts})

                    elif topic == 'agent/asr/intermediate':
                        if missionStartTime < 0 or expType != 2:
                            continue
                        # get timestamp and playername/participant_id
                        participant_id = data['participant_id']
                        callsign = self.getPlayerCallsignFromNameOrId(participant_id, participant_id, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            if players[callsign]['asr_ts'] < 0:
                                ts = self.dateStringToTimestamp(msg['msg']['timestamp'])
                                players[callsign]['asr_ts'] = ts - missionStartTime

                    elif topic == 'agent/asr/final':
                        if missionStartTime < 0:
                            continue
                        participant_id = data['participant_id']
                        callsign = self.getPlayerCallsignFromNameOrId(participant_id, participant_id, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            ts = players[callsign]['asr_ts']
                            if ts < 0:
                                if 'start_timestamp' in data:
                                    ts = self.dateStringToTimestamp(data['start_timestamp'])
                                else:
                                    ts = self.dateStringToTimestamp(msg['msg']['timestamp'])
                                ts = ts - missionStartTime
                            # last_elapsed_time = ts
                            timings.append(['asr', int(ts*1000), -1, 'at', callsign, data['text'].strip()])
                            # players[callsign]['asr'].append([int(round(ts*1000,0)), callsign, data['text'].strip()])
                            players[callsign]['asr_ts'] = -1

                    elif topic.startswith("agent/intervention/") and topic.endswith("/chat"):
                        source = topic.split("/")[2]
                        ts = last_elapsed_time
                        timings.append(['asr', int(ts), -1, 'at', "Int from " + source + " for " + str(data['receivers']), data['content'].strip()])

                    elif topic == 'minecraft/chat':
                        source = data['sender']
                        ts = last_elapsed_time
                        chatMsg = json.loads(data['text'].strip())
                        timings.append(['asr', int(ts), -1, 'at', "Chat For " + str(data['addressees']), "Color[" + chatMsg['color'] + "] Msg: '" + chatMsg['text'] + "'"])

                    # elif topic == 'observations/events/player/jag':
                    #     callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                    #     if callsign is not None and callsign in players.keys():
                    #         player = players[callsign]
                    #         ts = last_elapsed_time
                    #         sub_type = msg['msg']['sub_type']
                    #         if 'jag' in data:
                    #             if 'elapsed_milliseconds' in data['jag']:
                    #                 ts = data['jag']['elapsed_milliseconds']
                    #             else:
                    #                 data['jag']['elapsed_milliseconds'] = ts
                    #             data['jag']['sub_type'] = sub_type
                    #             print("Calling updatePlayerJag from the message processing loop for : " + data['jag']['id'] + " subtype: " + sub_type)
                    #             ASISTDataTools.updatePlayerJag(player, data['jag'])

                if generateSVG:
                    # generate SVG for the rubble
                    rubbleSVG, rubbleTimings = generateRubbleSVG(rubble, xtrans, ztrans)
                    timings.extend(rubbleTimings)
                    # generate SVG for the threats
                    threatSVG, threatTimings = generateThreatSVG(threats, xtrans, ztrans)
                    timings.extend(threatTimings)
                    # generate SVG for the markers
                    markerSVG, markerTimings = generateMarkerBlockSVG(markers, xtrans, ztrans)
                    timings.extend(markerTimings)
                    # generate SVG for the victims
                    victimsSVG, victimTimings = generateVictimSVG(victims, xtrans, ztrans)
                    timings.extend(victimTimings)
                    # generate SVG for the players (path, location, role)
                    playerSVG = []
                    pathSVG = []

                    print("{")
                    for player in players.values():
                        # print('"' + player['callsign'] + '":')
                        # print(json.dumps(player['unprocessed_jags']))
                        # print(',')
                        pthSVG, plrSVG, plrTimings = generatePlayerSVG(player, xtrans, ztrans)
                        timings.extend(plrTimings)
                        playerSVG.append(plrSVG)
                        pathSVG.append(pthSVG)
                    # print("}")

                    # print(sub_types)
                    # print(urns)

                    timings = sorted(timings, key=lambda d: d[1])
                    for timing in timings:
                        if timing[0] == 'mission_score':
                            timing[4] = str(timing[4]) + ' of ' + str(teamScore)

                # for callsign in ['red', 'green', 'blue']:  # ['team']:  # 
                #     print("=====================================")
                #     jah.player_summary (callsign)
                #     print("=====================================")

                # generate the base CSS, Groups, and timeline items
                newTimelineData = generateTimelineData(players, timeline, basemap, rubble, victims, expType, twentyMinutesAsMS, jah, planning_stop_time,markers)
                if 'Annotations' in timelineData.keys():
                    newTimelineData['Annotations'] = timelineData['Annotations']
                timelineData = newTimelineData

            if generateSVG:
                basemapSVG = ''
                if basemap is not None:
                    basemapSVG = BaseMap2SVG(basemap, xtrans, ztrans)

                svg_map += semanticMapSVG + "\n"
                svg_map += basemapSVG + "\n"
                svg_map += rubbleSVG + "\n"
                svg_map += markerSVG + "\n"
                svg_map += threatSVG + "\n"
                svg_map += victimsSVG + "\n"
                for path in pathSVG:
                    svg_map += path + "\n"
                for player in playerSVG:
                    svg_map += player + "\n"
                        
                scale = 5

            # write out the generated info so it does not have to be generated again.
            # print("Saving MapTools data...")
            # trial_info['mapSearchTools'].initialize(basemap, rubble, victims)
            # trial_info['mapSearchTools'].save(trial_info['metadata-filename'] + '.maptools')

            if generateSVG:
                svg = '<svg height="' + str(height*scale) + '" width="' + str(width*scale) + '" xmlns="http://www.w3.org/2000/svg" ' + '>\n'+ \
                        '<g transform="scale(' + str(scale) + ')">\n' + \
                            '<rect id="x_' + str(xtrans) + '_z_' + str(ztrans) + '" x="0" y="0" width="' + str(width) + '" height="' + str(height) + '" style="fill:rgb(255,255,255)"></rect>\n' + \
                            svg_map + \
                    '</g></svg>'
                f = open(trial_info['metadata-filename'] + ".svg", "w")
                f.write(svg)
                f.close()

                f = open(trial_info['metadata-filename'] + ".timings", "w")
                f.write(json.dumps(timings))
                f.close()

            if len(timelineData) > 0:
                f = open(trial_info['metadata-filename'] + ".timeline", "w")
                f.write(json.dumps(timelineData))
                f.close()

        return width, height, xtrans, ztrans, svg_map, timings, timelineData

    # @staticmethod
    # def getChildJag(parent, id):
    #     if parent['id'] == id:
    #         return parent
    #     if 'children' in parent and len(parent['children']) > 0:
    #         for ch in parent['children']:
    #             child = ASISTDataTools.getChildJag(ch, id)
    #             if child is not None:
    #                 return child
    #     return None

    # @staticmethod
    # def updatePlayerJag(player, jag, alreadyProcessed = False):
    #     print("- Updating " + player['callsign'] + "'s jags with jagID: " + jag['id'])
    #     if 'unprocessed_jags' not in player:
    #         player['unprocessed_jags'] = []

    #     if jag['sub_type'] == 'Event:Discovered':
    #         print(" - Adding Discovery Event: " + jag['id'])
    #         player['jag'][jag['id']] = jag
    #         toRemove = []
    #         print("   - processing unprocessed jags...")
    #         for uj in player['unprocessed_jags']:
    #             if ASISTDataTools.updatePlayerJag(player, uj, True):
    #                 toRemove.append(uj)
    #         print("   - Removing processed jags from the unprocessed list...")
    #         for uj in toRemove:
    #             player['unprocessed_jags'].remove(uj)
    #         return True

    #     # find a discovery event which contains this jag's id:
    #     print(" - Not a Discovery event so looking for it's parent...")
    #     for deid in player['jag']:
    #         de = player['jag'][deid]
    #         child = ASISTDataTools.getChildJag(de, jag['id'])
    #         if child is not None:
    #             if 'jags' not in child:
    #                 child['jags'] = []
    #             child['jags'].append(jag)
    #             return True

    #     if not alreadyProcessed:
    #         # No corresponding Discovery event yet, so store this for later:
    #         print(" - No parent Discovery so adding to the unprocessed jags list. ")
    #         player['unprocessed_jags'].append(jag)

    #     return False

    #     # if sub_type not in player['jag']:
    #     #     player['jag'][sub_type] = {}
    #     # if data['jag']['id'] in player['jag'][sub_type]:
    #     #     print("** Id [" + data['jag']['id'] + "] already in subtype [" + sub_type + "]")
    #     # player['jag'][sub_type][data['jag']['id']] = data['jag']
        
    #     # if 'urn' in data['jag'] and data['jag']['urn'] not in urns:
    #     #     urns.append(data['jag']['urn'])
    #     # if sub_type not in sub_types:
    #     #     sub_types.append(sub_type)

    #     # ['Event:Discovered', 
    #     #       'Event:Awareness', 
    #     #       'Event:Completion', 
    #     #       'Event:Addressing', 
    #     #       'Event:Preparing']
    #     # ['urn:ihmc:asist:search-area', 
    #     # 'urn:ihmc:asist:get-in-range', 
    #     # 'urn:ihmc:asist:unlock-victim', 
    #     # 'urn:ihmc:asist:rescue-victim', 
    #     # 'urn:ihmc:asist:check-if-unlocked', 
    #     # 'urn:ihmc:asist:at-proper-triage-area', 
    #     # 'urn:ihmc:asist:stabilize', 
    #     # 'urn:ihmc:asist:diagnose', 
    #     # 'urn:ihmc:asist:pick-up-victim', 
    #     # 'urn:ihmc:asist:drop-off-victim', 
    #     # 'urn:ihmc:asist:clear-path']

    # def getShortestPath(self, trial_info, pt1, pt2, time):
    #     return trial_info['mapSearchTools'].shortestPath(pt1, pt2, time)

    # dtools = ASISTDataTools(maps_folder='maps', data_folder='data')
    # trials = dtools.getListOfTrials()

    # print(trials[-1]['name'])
    # s_svg_map, s_timings = dtools.loadTrial(trials[-1])
