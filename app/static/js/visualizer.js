// Socket io
let socket = null;

// Falcon settings - These values are recomputed when the mission data is received.
let map_width = 2050;
let map_height = 800;
let xtrans = 0;
let ztrans = 0;
let scale = 6;
let rotate90 = false;

let svg = ''

let path_svg_ids = [];
let timings = [];
let timelineData = [];
let eventIds = [];
let cssTypes = {};

let trial_data = {};
let state_info = {'index': -1, 'current_time': 0}

let video_start_time = 0;
let update_timer = null;
let videojs_player = null;
let timeline = null;
let timelineMovingTime = false;
let diffTime = 0;
const maxMinutesAsMS = 18*60*1000;

trial_list = [];
trialFilter = {
    'text': '',
    'hsrOnly': false,
    'trialsOnly': false
};
newItemDefaults = {
    'type': '',
    'duration': 0,
    'event-id': ''
}

let selectedGroup = 'Teamwork';
let updatedGroups = [];

let filters = {"types": [], "groups": []}

let selectedTrial = 'default';
let static_root = 'app/static';

let addedCSS = {};

let fontDataList = '<datalist id="font-list">' +
                        '<option value="Arial">' +
                        '<option value="Arial Black">' +
                        '<option value="Verdana">' +
                        '<option value="Tahoma">' +
                        '<option value="Trebuchet MS">' +
                        '<option value="Impact">' +
                        '<option value="Times New Roman">' +
                        '<option value="Didot">' +
                        '<option value="Georgia">' +
                        '<option value="American Typewriter">' +
                        '<option value="Andalé Mono">' +
                        '<option value="Courier">' +
                        '<option value="Lucida Console">' +
                        '<option value="Monaco">' +
                        '<option value="Bradley Hand">' +
                        '<option value="Brush Script MT">' +
                        '<option value="Luminari">' +
                        '<option value="Comic Sans MS">' +
                    '</datalist>';
let colorDataList = '<datalist id="color-list">' +
                        '<option value="MediumVioletRed">' +
                        '<option value="DeepPink">' +
                        '<option value="PaleVioletRed">' +
                        '<option value="HotPink">' +
                        '<option value="LightPink">' +
                        '<option value="Pink">' +
                        '<option value="DarkRed">' +
                        '<option value="Red">' +
                        '<option value="Firebrick">' +
                        '<option value="Crimson">' +
                        '<option value="IndianRed">' +
                        '<option value="LightCoral">' +
                        '<option value="Salmon">' +
                        '<option value="DarkSalmon">' +
                        '<option value="LightSalmon">' +
                        '<option value="OrangeRed">' +
                        '<option value="Tomato">' +
                        '<option value="DarkOrange">' +
                        '<option value="Coral">' +
                        '<option value="Orange">' +
                        '<option value="DarkKhaki">' +
                        '<option value="Gold">' +
                        '<option value="Khaki">' +
                        '<option value="PeachPuff">' +
                        '<option value="Yellow">' +
                        '<option value="PaleGoldenrod">' +
                        '<option value="Moccasin">' +
                        '<option value="PapayaWhip">' +
                        '<option value="LightGoldenrodYellow">' +
                        '<option value="LemonChiffon">' +
                        '<option value="LightYellow">' +
                        '<option value="Maroon">' +
                        '<option value="Brown">' +
                        '<option value="SaddleBrown">' +
                        '<option value="Sienna">' +
                        '<option value="Chocolate">' +
                        '<option value="DarkGoldenrod">' +
                        '<option value="Peru">' +
                        '<option value="RosyBrown">' +
                        '<option value="Goldenrod">' +
                        '<option value="SandyBrown">' +
                        '<option value="Tan">' +
                        '<option value="Burlywood">' +
                        '<option value="Wheat">' +
                        '<option value="NavajoWhite">' +
                        '<option value="Bisque">' +
                        '<option value="BlanchedAlmond">' +
                        '<option value="Cornsilk">' +
                        '<option value="DarkGreen">' +
                        '<option value="Green">' +
                        '<option value="DarkOliveGreen">' +
                        '<option value="ForestGreen">' +
                        '<option value="SeaGreen">' +
                        '<option value="Olive">' +
                        '<option value="OliveDrab">' +
                        '<option value="MediumSeaGreen">' +
                        '<option value="LimeGreen">' +
                        '<option value="Lime">' +
                        '<option value="SpringGreen">' +
                        '<option value="MediumSpringGreen">' +
                        '<option value="DarkSeaGreen">' +
                        '<option value="MediumAquamarine">' +
                        '<option value="YellowGreen">' +
                        '<option value="LawnGreen">' +
                        '<option value="Chartreuse">' +
                        '<option value="LightGreen">' +
                        '<option value="GreenYellow">' +
                        '<option value="PaleGreen">' +
                        '<option value="Teal">' +
                        '<option value="DarkCyan">' +
                        '<option value="LightSeaGreen">' +
                        '<option value="CadetBlue">' +
                        '<option value="DarkTurquoise">' +
                        '<option value="MediumTurquoise">' +
                        '<option value="Turquoise">' +
                        '<option value="Aqua">' +
                        '<option value="Cyan">' +
                        '<option value="Aquamarine">' +
                        '<option value="PaleTurquoise">' +
                        '<option value="LightCyan">' +
                        '<option value="Navy">' +
                        '<option value="DarkBlue">' +
                        '<option value="MediumBlue">' +
                        '<option value="Blue">' +
                        '<option value="MidnightBlue">' +
                        '<option value="RoyalBlue">' +
                        '<option value="SteelBlue">' +
                        '<option value="DodgerBlue">' +
                        '<option value="DeepSkyBlue">' +
                        '<option value="CornflowerBlue">' +
                        '<option value="SkyBlue">' +
                        '<option value="LightSkyBlue">' +
                        '<option value="LightSteelBlue">' +
                        '<option value="LightBlue">' +
                        '<option value="PowderBlue">' +
                        '<option value="Indigo">' +
                        '<option value="Purple">' +
                        '<option value="DarkMagenta">' +
                        '<option value="DarkViolet">' +
                        '<option value="DarkSlateBlue">' +
                        '<option value="BlueViolet">' +
                        '<option value="DarkOrchid">' +
                        '<option value="Fuchsia">' +
                        '<option value="Magenta">' +
                        '<option value="SlateBlue">' +
                        '<option value="MediumSlateBlue">' +
                        '<option value="MediumOrchid">' +
                        '<option value="MediumPurple">' +
                        '<option value="Orchid">' +
                        '<option value="Violet">' +
                        '<option value="Plum">' +
                        '<option value="Thistle">' +
                        '<option value="Lavender">' +
                        '<option value="MistyRose">' +
                        '<option value="AntiqueWhite">' +
                        '<option value="Linen">' +
                        '<option value="Beige">' +
                        '<option value="WhiteSmoke">' +
                        '<option value="LavenderBlush">' +
                        '<option value="OldLace">' +
                        '<option value="AliceBlue">' +
                        '<option value="Seashell">' +
                        '<option value="GhostWhite">' +
                        '<option value="Honeydew">' +
                        '<option value="FloralWhite">' +
                        '<option value="Azure">' +
                        '<option value="MintCream">' +
                        '<option value="Snow">' +
                        '<option value="Ivory">' +
                        '<option value="White">' +
                        '<option value="Black">' +
                        '<option value="DarkSlateGray">' +
                        '<option value="DimGray">' +
                        '<option value="SlateGray">' +
                        '<option value="Gray">' +
                        '<option value="LightSlateGray">' +
                        '<option value="DarkGray">' +
                        '<option value="Silver">' +
                        '<option value="LightGray">' +
                        '<option value="Gainsboro">' +
                    '</datalist>';

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
                      
$(document).ready(function(){

    //connect to the socket server.
    console.log('creating the socket...');
    socket = io.connect('http://' + document.domain + ':' + location.port + '/ihmc_replay_visualizer', { transport : ['websocket'] });
    console.log(' - finished creating the socket.');

    //setup methods to receive trial data from server the server
    socket.on('trial_data', function(msg){ sio_trial_data(msg); });
    socket.on('trial_list', function(msg){ sio_trial_list(msg); });
    socket.on('timeline_types', function(msg){ sio_timeline_types(msg); });

    // request the trial list from the server
    console.log('  - requesting trial list...');
    socket.emit('get_trial_list', {}, namespace='/ihmc_replay_visualizer');
    socket.emit('get_timeline_types', {}, namespace='/ihmc_replay_visualizer');

    update_timer = setInterval(updateStateInfo, 100);
    videojs_player = videojs('my-video');

    video_start_time = 0;
    state_info = {'current_time': 0, 'video_start_time': 0, 'players':{}, 'index': -1};

    date = new Date("Jan 1, 1970");
    diffTime = date.getTime();  // 64800000
    // // diffTime = date.getUTCMilliseconds() + (date.getUTCSeconds() + (date.getUTCMinutes() * 60)) * 1000;
    // console.log(date);
    // console.log(date.getTime());
    // console.log(date.getUTCMilliseconds() + (date.getUTCSeconds() + (date.getUTCMinutes() * 60)) * 1000);

    $("#trial_load_progress").hide();
    $("#delete_selected").hide();
    $("#asr").html("<br>");
    $("#save-timeline").prop('disabled', true);
    $("#save-timeline-types").prop('disabled', true);

    var lsFilters = localStorage.getItem('vis_filters');
    if (lsFilters != null)
        filters = JSON.parse(lsFilters);

    var niStorage = localStorage.getItem('new_timeline_item_defaults')
    if (niStorage != null)
        newItemDefaults = JSON.parse(niStorage);

    setup_anotation_timeline();
});

function setup_anotation_timeline()
{
    // DOM element where the Timeline will be attached
    var container = document.getElementById('anotation-timeline');

    // createClassForTimelineItem ('rubble', null, '5', null,                  // name, height, z-index, background color
    //                             null, null, null, null,                     // border: color, width, radius, style
    //                             null, null, null,                           // font: color, family, size
    //                             '3px', '3px', 'rgb(100, 100, 100)', null,   // dot: radius, width, color, style
    //                             null, null, null);                          // line: width, color, style

    // createClassForTimelineItem ('triaged-victim', null, '5', 'rgb(0, 255, 0)', // name, height, z-index, background color
    //                             'rgb(230, 255, 15)', '3px', '3px', null,    // border: color, width, radius, style
    //                             null, null, null,                           // font: color, family, size
    //                             '1px', '4px', 'rgb(0, 255, 0)', null,       // dot: radius, width, color, style
    //                             '1px', 'rgb(0, 255, 0)', null);             // line: width, color, style

    // createClassForTimelineItem ('triaged-critical-victim', null, '5', 'rgb(255, 0, 0)', // name, height, z-index, background color
    //                             'rgb(230, 255, 15)', '3px', '3px', null,    // border: color, width, radius, style
    //                             null, null, null,                           // font: color, family, size
    //                             '1px', '4px', 'rgb(255, 0, 0)', null,       // dot: radius, width, color, style
    //                             '1px', 'rgb(255, 0, 0)', null);             // line: width, color, style

    // createClassForTimelineItem ('carrying-victim', null, '2', 'rgb(200, 200, 0)', // name, height, z-index, background color
    //                             'rgb(0, 0, 0)', '1px', '2px', null,     // border: color, width, radius, style
    //                             null, null, null,                       // font: color, family, size
    //                             null, null, null, null,                 // dot: radius, width, color, style
    //                             null, null, null);                      // line: width, color, style

    // createClassForTimelineItem ('frozen', null, '3', 'rgb(180, 180, 180)', // name, height, z-index, background color
    //                             'rgb(255, 0, 0)', '1px', '2px', null,     // border: color, width, radius, style
    //                             null, null, null,                       // font: color, family, size
    //                             null, null, null, null,                 // dot: radius, width, color, style
    //                             null, null, null);                      // line: width, color, style

    // createClassForTimelineItem ('triaging', null, '3', 'rgb(140, 185, 15)', // name, height, z-index, background color
    //                             'rgb(255, 0, 0)', '1px', '2px', null,     // border: color, width, radius, style
    //                             null, null, null,                       // font: color, family, size
    //                             null, null, null, null,                 // dot: radius, width, color, style
    //                             null, null, null);                      // line: width, color, style

    var items = new vis.DataSet([
        {id: 'A', content: 'Period A', start: diffTime+(60000*2), end: maxMinutesAsMS+diffTime-(60000*2), type: 'background', group: 1, style: 'background-color: red;'},
        {id: 2, content: '', start: diffTime+(60000*5), group: 1, className: 'triaged-victim'},
        {id: 3, content: '', start: diffTime+(60000*1), group: 1, className: 'triaged-critical-victim'},
        {id: 32, content: 'CV', start: diffTime+(60000*1), group: 2, className: 'triaged-critical-victim'},
        {id: 5, content: '', start: diffTime+(60000*12), type:'point', group: 2, className: 'rubble'},
        {id: 6, content: 'RBL', start: diffTime+(60000*12), type:'point', group: 3, className: 'rubble'},
        {id: 4, content: 'guide', start: diffTime+(60000*6), end: diffTime+(60000*10), group: 2},
        {id: 45, content: '', start: diffTime+(60000*6), end: diffTime+(60000*10), group: 3, className: 'carrying-victim'}
    ]);
    var groups = new vis.DataSet([
        {id: 1, order: 1, content: 'Red'},
        {id: 2, order: 2, content: 'Green'},
        {id: 3, order: 3, content: 'Blue'},
        {id: 0, order: 99, content: 'Score'}
    ]);

    // Configuration for the Timeline
    var options = {
        start: 0+diffTime,
        end: maxMinutesAsMS+diffTime,

        min: 0+diffTime,
        max: maxMinutesAsMS+diffTime,
    
        // allow selecting multiple items using ctrl+click, shift+click, or hold.
        multiselect: true,
    
        // allow manipulation of items
        editable: true,

        align: 'left',
    
        showCurrentTime: false,
        showMajorLabels: false,
        snap: null,
        stack: true,
        stackSubgroups: true,
        onAdd: function (item, callback) {handleAddItem(item, callback);},
        onUpdate: function (item, callback) {handleUpdateItem(item, callback);},
        onMove:  function (item, callback) {handleMoveItem(item, callback);},
        onRemove:  function (item, callback) {handleRemoveItem(item, callback);}
      };
    
    // Create a Timeline
    timeline = new vis.Timeline(container, items, groups, options);
    timeline.addCustomTime(695200+diffTime, 1)
    timeline.on ('timechanged', timelineTimechanged);
    timeline.on ('timechange', timelineTimechange);
    timeline.on ('click', handleClickItem);
}

async function handleAddItem(item, callback)
{
    //console.log('Adding Item with id: ' + item['id']);
    item.content = '';
    item.subgroup = 'sg1';
    item.subgroupOrder = 1;
    seconds = (item.start.getTime()-diffTime)/1000.0;
    minutes = Math.floor(seconds/60);
    seconds = (seconds - (minutes*60)).toFixed(3);
    options = '';
    if (cssTypes && 'added' in cssTypes) {
        typeNames = []
        cssTypes['added'].forEach (type => {
            typeNames.push(type['name']);
        });
        typeNames.sort().forEach (type => {
            if (type == newItemDefaults['type'])
                options += '<option selected value="'+ type +'">'+ type +'</option>';
            else
                options += '<option value="'+ type +'">'+ type +'</option>';
        });
    }
    eventIdDataList = '<datalist id="event-ids">';
    if (eventIds.length > 0) {
        eventIds.forEach (id => {
            eventIdDataList += '<option value="'+ id +'">';
        });
    }
    eventIdDataList += '</datalist>';

    const { value: formValues } = await Swal.fire({
        title: 'Add annotation',
        width: '600px',
        html:
            '<div style="width:400px;float:left;">' + 
                '<label style="width:100px;text-align:right;">Type:</label>' +
                '<select id="swal-type" class="swal2-select" style="width:50px;">' +
                    options +
                '</select>' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Name:</label>' +
                '<input id="swal-content" class="swal2-input">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Hover Text:</label>' +
                '<input id="swal-title" class="swal2-input">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Start Time:</label>' +
                '<input id="swal-time-min"  type="number" class="swal2-input" style="width:90px;" value="' + minutes + '">' +
                '<label style="width:1px;text-align:right;">:</label>' +
                '<input id="swal-time-sec" type="number" class="swal2-input" style="width:140px;" value="' + seconds + '">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Duration:</label>' +
                '<input id="swal-time-duration" type="number" class="swal2-input" value="' + newItemDefaults['duration'] + '">' +
                '<label style="width:100px;text-align:right;">(seconds)</label>' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Event ID:</label>' +
                '<input id="swal-evtId" class="swal2-input" placeholder="Event Id" list="event-ids" value="' + newItemDefaults['event-id'] + '">' +
            '</div>' +
            eventIdDataList,
        focusConfirm: false,
        preConfirm: () => {
          event_id = document.getElementById('swal-evtId').value.trim();
          return {
            'content': document.getElementById('swal-content').value.trim(),
            'title': document.getElementById('swal-title').value.trim(),
            'minutes': document.getElementById('swal-time-min').value,
            'seconds': document.getElementById('swal-time-sec').value,
            'duration': document.getElementById('swal-time-duration').value,
            'type': document.getElementById('swal-type').value.trim(),
            'event-id': event_id
          }
        }
      })
      
    if (formValues) {
        //console.log(formValues);
        item.content = formValues['content'];
        if (formValues['title'].length > 0)
            item.title = formValues['title'];
        minutes = parseFloat(formValues['minutes']);
        seconds = parseFloat(formValues['seconds']);
        duration = parseFloat(formValues['duration']);
        item.start = (((minutes * 60) + seconds) * 1000) + diffTime;
        if (item.start < diffTime)
            item.start = diffTime;
        if (duration > 0)
            item.end = item.start + (duration * 1000);
            if (item.end > diffTime + maxMinutesAsMS)
                item.end = diffTime + maxMinutesAsMS;
        item.className = formValues['type'];
        if (item.content.length <= 0)
            item.content = item.className;
        if (formValues['event-id'].length > 0) {
            item['event-id'] = formValues['event-id'];
            if (!eventIds.includes(item['event-id'])) {
                eventIds.push(item['event-id']);
                eventIds = eventIds.sort();
            }
        }
        if (!('Annotations' in timelineData))
            timelineData['Annotations'] = {};
        if (!(selectedGroup in timelineData['Annotations']))
            timelineData['Annotations'][selectedGroup] = {};

        timelineData['Annotations'][selectedGroup][item['id']] = item;
        if (!updatedGroups.includes(selectedGroup))
            updatedGroups.push(selectedGroup);
        console.log("Added Annotation to : " + selectedGroup);
        //console.log(timelineData['Annotations']);

        newItemDefaults['type'] = formValues['type'];
        newItemDefaults['duration'] = duration;
        newItemDefaults['event-id'] = formValues['event-id'];
        localStorage.setItem('new_timeline_item_defaults', JSON.stringify(newItemDefaults));

        callback(item); // send back adjusted item
        $("#save-timeline").prop('disabled', false);
    }
    else {
        callback(null); // cancel updating the item
    }
}

async function generateEditCSSClass(cssClass, nameEditable=true)
{
    let name = cssClass['name'] != null ? cssClass['name'] : '';
    let background_color = cssClass['background-color'] != null ? cssClass['background-color'] : '';
    let border_color = cssClass['border-color'] != null ? cssClass['border-color'] : '';
    let border_width = cssClass['border-width'] != null ? cssClass['border-width'] : '';
    let font_color = cssClass['font-color'] != null ? cssClass['font-color'] : '';
    let font_family = cssClass['font-family'] != null ? cssClass['font-family'] : '';
    let font_size = cssClass['font-size'] != null ? cssClass['font-size'] : '';
    let z_index = cssClass['z-index'] != null ? cssClass['z-index'] : '10';

    nameReadOnlyValue = '';
    if (!nameEditable)
    nameReadOnlyValue = ' readonly';

    border_width = border_width.replace('px', '');
    font_size = font_size.replace('pt', '');

    const { value: formValues } = await Swal.fire({
        title: 'Annotation Type',
        width: '600px',
        html:
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Name:</label>' +
                '<input id="swal-name" class="swal2-input" placeholder="Type Name (Required)!!" value="' + name + '"' + nameReadOnlyValue + '>' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Bkg Color:</label>' +
                '<input id="swal-bkg-color" class="swal2-input" placeholder="rgb(255,255,255)" value="' + background_color + '" list="color-list">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Border Color:</label>' +
                '<input id="swal-bdr-color" class="swal2-input" placeholder="rgb(0,0,0)" value="' + border_color + '" list="color-list">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Border Width:</label>' +
                '<input id="swal-bdr-width" type="number" class="swal2-input" placeholder="2" value="' + border_width + '">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Font Color:</label>' +
                '<input id="swal-font-color" class="swal2-input" placeholder="rgb(0,0,0)" value="' + font_color + '" list="color-list">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Font Family:</label>' +
                '<input id="swal-font-family" class="swal2-input" placeholder="Arial" value="' + font_family + '" list="font-list">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Font Size:</label>' +
                '<input id="swal-font-size" type="number" class="swal2-input" placeholder="10" value="' + font_size + '">' +
            '</div>' +
            fontDataList + colorDataList,
        focusConfirm: false,
        preConfirm: () => {
          return {
            'name': document.getElementById('swal-name').value.trim().replace(/\s/g, '_'),  
            'background-color': document.getElementById('swal-bkg-color').value.trim(),
            'border-color': document.getElementById('swal-bdr-color').value.trim(),
            'border-width': document.getElementById('swal-bdr-width').value.trim() + 'px',
            'font-color': document.getElementById('swal-font-color').value.trim(),
            'font-family': document.getElementById('swal-font-family').value.trim(),
            'font-size': document.getElementById('swal-font-size').value.trim() + 'pt',
            'z-index': z_index
          }
        }
      })
      
    if (formValues) {
        console.log(formValues['background-color2']);
        cssClass['name'] = formValues['name'];
        if (formValues['background-color'] == '') {
            if('background-color' in cssClass)
                delete cssClass['background-color'];
        }
        else 
            cssClass['background-color'] = formValues['background-color'];
        if (formValues['border-color'] == '') {
            if('border-color' in cssClass)
                delete cssClass['border-color'];
        }
        else 
            cssClass['border-color'] = formValues['border-color'];
        if (formValues['border-width'] == 'px' || formValues['border-width'] == '0px') {
            if('border-width' in cssClass)
                delete cssClass['border-width'];
        }
        else 
            cssClass['border-width'] = formValues['border-width'];
        if (formValues['font-color'] == '') {
            if('font-color' in cssClass)
                delete cssClass['font-color'];
        }
        else 
            cssClass['font-color'] = formValues['font-color'];
        if (formValues['font-family'] == '') {
            if('font-family' in cssClass)
                delete cssClass['font-family'];
        }
        else 
            cssClass['font-family'] = formValues['font-family'];
        if (formValues['font-size'] == 'pt' || formValues['font-size'] == '0pt') {
            if('font-size' in cssClass)
                delete cssClass['font-size'];
        }
        else 
            cssClass['font-size'] = formValues['font-size'];
        cssClass['z-index'] = formValues['z-index'];
        cssClass['updated'] = true;
    }
}

async function newTimelineType()
{
    cssType = {'name': ''};

    // Edit Selected CSS and save changes back to the server.
    while (cssType['name'] == '') {
        cssType['updated'] = false;
        await generateEditCSSClass(cssType);
        updated = cssType['updated'];
        delete cssType['updated'];
        if (!updated)
            return;

        if (cssType['name'] == '') {
            await Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'A Name is requried.  Please set the name.'
              });
            cssType['name'] = '';
            continue;
        }
    
        // verify that the name does not conflict with other names
        for (css of cssTypes['defaults']) {
            if (css['name'].toLowerCase() == cssType['name'].toLowerCase()) {
                await Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please choose another name.  This one matches an exiting default type.'
                  });
                cssType['name'] = '';
                continue;
            }
        }
        for (css of cssTypes['added']) {
            if (css['name'].toLowerCase() == cssType['name'].toLowerCase()) {
                await Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please choose another name.  This one matches an exiting type.'
                  });
                cssType['name'] = '';
                continue;
            }
        }
    }

    // Add the new CSS to the HTML Doc Head.
    createClassForTimeLineItemFromCSS(cssType);
    cssTypes['added'].push(cssType);
    updateCSSselection();
    $("#save-timeline-types").prop('disabled', false);
}

function saveTimelineTypes()
{
    //console.log("Saving Types:");
    //console.log(cssTypes);
    socket.emit('save_timeline_types', cssTypes, namespace='/ihmc_replay_visualizer');
    $("#save-timeline-types").prop('disabled', true);
}

function getUpdatedTimelineDataForSaving(timelineItems, removeId=false)
{
    // Send the updated annotation/timeline data back to the server...
    updates = []
    if (timelineItems != null) {
        for (id in timelineItems) {
            item = timelineItems[id];
            if (item == null) 
                continue;
            update = {};
            for (key in timelineItems[id]) {
                if (key == 'start' || key == 'end') {
                    if (timelineItems[id][key] != null)
                       update[key] = item[key]-diffTime;
                }
                else if (removeId && key == 'id') {
                    continue;
                }
                else if (timelineItems[id][key] != null) {
                    update[key] = item[key];
                }
            }
            updates.push(update);
        }
    }
    return updates;
}

function saveTimelineData()
{
    //console.log(timelineData['Annotations']);
    if (!('Annotations' in timelineData))
        return;

    // Send the updated annotation/timeline data back to the server...
    annotations = {}
    for (group in timelineData['Annotations']) {
        annotations[group] = getUpdatedTimelineDataForSaving(timelineData['Annotations'][group]);
    }

    selectedTrial = document.getElementById("trials").value;
    msg = {'trial': selectedTrial, 'annotations': annotations, 'updated': updatedGroups};
    socket.emit('update_annotations', msg, namespace='/ihmc_replay_visualizer');
    $("#save-timeline").prop('disabled', true);
    updatedGroups = []
}

function saveFile(fileName,urlFile)
{
    let a = document.createElement("a");
    a.style = "display: none";
    document.body.appendChild(a);
    a.href = urlFile;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
}

function exportTimelineData()
{
    // Send the updated annotation/timeline data back to the server...
    updates = {"Groups": timelineData['Groups'], 
               "Items": getUpdatedTimelineDataForSaving(timelineData['Items']), 
               "Annotations": {}
    };
    if (timelineData['Annotations']) {
        for (group in timelineData['Annotations']) {
            updates['Annotations'][group] = getUpdatedTimelineDataForSaving(timelineData['Annotations'][group]);
        }
    }
    json = JSON.stringify(updates, undefined, 2);

    let blobData = new Blob([json], {type: "application/json"});
    let url = window.URL.createObjectURL(blobData);
    annotationsFileName = document.getElementById("trials").value + ".json"
    saveFile(annotationsFileName, url);
}

async function setTrialListFilter()
{
    text = trialFilter['text']
    hsrChecked = (trialFilter['hsrOnly']?'checked':'')
    trialChecked = (trialFilter['trialsOnly']?'checked':'')

    const { value: formValues } = await Swal.fire({
        title: 'Trial Filters',
        width: '600px',
        html:
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Text:</label>' +
                '<input id="swal-text" class="swal2-input" placeholder="Filter Text" value="' + text + '">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;"></label>' +
                '<input id="swal-hsr" class="swal2-checkbox" type="checkbox" value="HSROnly" ' + hsrChecked + '>' +
                '<label style="width:200px;text-align:left;">HSR Data Only</label>' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;"></label>' +
                '<input id="swal-trials" class="swal2-checkbox" type="checkbox" value="trialsOnly" ' + trialChecked + '>' +
                '<label style="width:200px;text-align:left;">Trial Data Only</label>' +
            '</div>',
        focusConfirm: false,
        preConfirm: () => {
          return {
            'text': document.getElementById('swal-text').value.trim().replace(/\s/g, '_'),  
            'hsrOnly': document.getElementById('swal-hsr').checked,
            'trialsOnly': document.getElementById('swal-trials').checked
          }
        }
      })
      
    if (formValues) {
        if (formValues['text'] != trialFilter['text'] ||
            formValues['hsrOnly'] != trialFilter['hsrOnly'] ||
            formValues['trialsOnly'] != trialFilter['trialsOnly'])
        {
            trialFilter = formValues;
            updateTrialsSelection();
        }
    
    }
}

function showPlayerInfo()
{
    console.log("Need to show player info!!");
}

async function handleUpdateItem(item, callback)
{
    if (!timelineData['Annotations'])
        return

    //console.log(item);
    if (!item.content || item.content == null) {
        item.content = '';
    }
    s_seconds = (item.start.getTime()-diffTime)/1000.0;

    duration = 0;
    if (item.end && item.end != null) {
        duration = ((item.end.getTime()-diffTime)/1000.0 - s_seconds).toFixed(3);
    }
    title = '';
    if (item.title && item.title != null) {
        title = item.title;
    }

    s_minutes = Math.floor(s_seconds/60);
    s_seconds = (s_seconds - (s_minutes*60)).toFixed(3);
    options = '';
    if (cssTypes && 'added' in cssTypes) {
        typeNames = []
        cssTypes['added'].forEach (type => {
            typeNames.push(type['name']);
        });
        typeNames.sort().forEach (type => {
            if (type == item.className)
                options += '<option selected value="'+ type +'">'+ type +'</option>\n';
            else
                options += '<option value="'+ type +'">'+ type +'</option>\n';
        });
    }
    event_id = '';
    if ('event-id' in item && item['event-id'] != null) {
        event_id = item['event-id'];
    }
    eventIdDataList = '<datalist id="event-ids">';
    if (eventIds.length > 0) {
        eventIds.forEach (id => {
            eventIdDataList += '<option value="'+ id +'">';
        });
    }
    eventIdDataList += '</datalist>';
    // console.log(item.className);
    // console.log(options);
    const { value: formValues } = await Swal.fire({
        title: 'Add annotation',
        width: '600px',
        html:
            '<div style="width:400px;float:left;">' + 
                '<label style="width:100px;text-align:right;">Type:</label>' +
                '<select id="swal-type" class="swal2-select" style="width:50px;">' +
                    options +
                '</select>' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Name:</label>' +
                '<input id="swal-content" class="swal2-input" value="' + item.content + '"><br>' +
            '</div>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Hover Text:</label>' +
                '<input id="swal-title" class="swal2-input" value="' + title + '"><br>' +
            '</div>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Start Time:</label>' +
                '<input id="swal-time-min"  type="number" class="swal2-input" style="width:90px;" value="' + s_minutes + '">' +
                '<label style="width:1px;text-align:right;">:</label>' +
                '<input id="swal-time-sec" type="number" class="swal2-input" style="width:140px;" value="' + s_seconds + '"><br>' +
            '</div>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Duration:</label>' +
                '<input id="swal-time-duration" type="number" class="swal2-input" value="' + duration + '">' +
            '</div><br>' +
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Event ID:</label>' + 
                '<input id="swal-evtId" class="swal2-input" placeholder="Event Id" value="' + event_id + '" list="event-ids">' +
            '</div>' +
            eventIdDataList,
        focusConfirm: false,
        preConfirm: () => {
            return {
            'content': document.getElementById('swal-content').value.trim(),
            'title': document.getElementById('swal-title').value.trim(),
            'minutes': document.getElementById('swal-time-min').value,
            'seconds': document.getElementById('swal-time-sec').value,
            'duration': document.getElementById('swal-time-duration').value,
            'type': document.getElementById('swal-type').value.trim(),
            'event-id': document.getElementById('swal-evtId').value.trim()
          }
        }
      })
      
    if (formValues) {
        //console.log(formValues);
        item.content = formValues['content'];
        if (formValues['title'].length > 0)
            item.title = formValues['title'];
        else
            item.title = null;
        minutes = parseFloat(formValues['minutes']);
        seconds = parseFloat(formValues['seconds']);
        duration = parseFloat(formValues['duration']);
        item.start = (((minutes * 60) + seconds) * 1000) + diffTime;
        if (item.start < diffTime)
            item.start = diffTime;
        if (duration > 0) {
            item.end = item.start + (duration * 1000);
        }
        else {
            item.end = null;
        }
        // console.log('duration = ' + duration);
        // console.log('item.end = ' + item.end);
        item.className = formValues['type'];

        if (formValues['event-id'].length > 0) {
            item['event-id'] = formValues['event-id'];
            if (!eventIds.includes(item['event-id'])) {
                eventIds.push(item['event-id']);
                eventIds = eventIds.sort();
            }
        }
        else if ('event-id' in item)
            delete item['event-id'];

        callback(item); // send back adjusted item
        // Find the item in it's list and update it.
        for (group in timelineData['Annotations']) {
            if (timelineData['Annotations'][group][item['id']]) {
                timelineData['Annotations'][group][item['id']] = item;
                if (!updatedGroups.includes(group))
                    updatedGroups.push(group);
                break;
            }
        }
        //console.log(timelineData['Annotations']);
        $("#save-timeline").prop('disabled', false);
    }
    else {
        callback(null); // cancel updating the item
    }
}

function handleMoveItem(item, callback)
{
    //console.log('Moved Item...');
    //console.log(item);

    start = item.start.getTime();
    end = -1;
    if (item.end)
        end = item.end.getTime();

    if (start < diffTime) {
        if (item.end) {
            item.end = end = diffTime + (end - start);
        }
        item.start = start = diffTime;
    }
    else if (start > diffTime + maxMinutesAsMS - 30000) {
        if (item.end) {
            item.end = end = diffTime + (end - start) + maxMinutesAsMS - 30000;
        }
        item.start = start = diffTime + maxMinutesAsMS - 30000;
    }
    callback(item);
    if (timelineData['Annotations']) {
        for (group in timelineData['Annotations']) {
            if (timelineData['Annotations'][group][item['id']]) {
                timelineData['Annotations'][group][item['id']] = item;
                if (!updatedGroups.includes(group))
                    updatedGroups.push(group);
                break;
            }
        }
    }
    //console.log("Moved Annotation: " + item['id']);
    //console.log(timelineData['Annotations']);
    $("#save-timeline").prop('disabled', false);
}

function handleRemoveItem(item, callback)
{
    //console.log('Removed Item...');
    //console.log(item);
    callback(item);
    if (timelineData['Annotations']) {
        for (group in timelineData['Annotations']) {
            if (timelineData['Annotations'][group][item['id']]) {
                delete timelineData['Annotations'][group][item['id']];
                if (!updatedGroups.includes(group))
                    updatedGroups.push(group);
                break;
            }
        }
    }
    //console.log("Removed Annotation: " + item['id']);
    //console.log(timelineData['Annotations']);
    $("#save-timeline").prop('disabled', false);
}

function handleClickItem(e)
{
    if (e.what == "group-label") {
        group = e.group;
        if (group == 0)
            return;

        parts = timelineData['Groups'][group]['title'].split(" ");
        if (parts.length < 3) 
            return;

        marker = static_root + 'images/MB_' + parts[3] + '.png';
        parts = parts[5].split("_");
        map = static_root + 'images/' + parts[0] + '_1.1_' + parts[1] + '.png';
        // console.log(marker + "  " + map)
        // swal.fire('testing 1 2 3...');
        swal.fire({
            title: 'Map and Marker Blocks Key',
            width: '600px',
            html: '<img id = "map" width="500px" src="' + map + '">' +
                  '<div><img id = "marker" width="500px" src="' + marker + '"></div>',
            preConfirm: () => {
                return;
            }
        })
    }
}

function timelineTimechange(e)
{
    timelineMovingTime = true;
        
    if (videojs_player == null)
        return;

    if (e['time'].getUTCMinutes() >= 20) {
        if (e['time'].getUTCMinutes() < 40) {
            tltime = maxMinutesAsMS;
        }
        else {
            tltime = 0;
        }
    }
    else {
        tltime = e['time'].getTime()-diffTime;
    }
    // console.log ("tc" + e['id'] + " [" + e['time'].getUTCMinutes()+":"+e['time'].getUTCSeconds()+"."+e['time'].getUTCMilliseconds()+ "] " + tltime);

    videojs_player.currentTime(tltime/1000.0);
}

function timelineTimechanged(e)
{
    timelineMovingTime = false;
}

function parseTrialName(trial)
{
    info = {"name": trial};
    parts = trial.split("_");
    if (parts.length < 2 || parts[1] != "TrialMessages")
        return info;

    info['trial'] = parts[2].split("-T000")[1];
    info['team'] = parts[3].split("-TM000")[1];
    return info;
}

function compareTrial(t1, t2)
{
    t1_info = parseTrialName(t1);
    t2_info = parseTrialName(t2);

    if (t1_info.hasOwnProperty('team') && t2_info.hasOwnProperty('team')) {
        if (t1_info.team < t2_info.team)
            return 1;
        if (t1_info.team > t2_info.team)
            return -1;
        if (t1_info.trial < t2_info.trial)
            return 1;
        if (t1_info.trial > t2_info.trial)
            return -1;
    }
    else {
        if (t1 > t2)
            return 1;
        if (t1 < t2)
            return -1;
    }
    return 0;
}

function sio_trial_list(msg)
{
    console.log('  - Received the trial list');
    console.log(msg)

    trial_list = msg['trial_list'];

    // sort the trial_list based on team and then trial in decending order
    trial_list.sort(compareTrial);

    updateTrialsSelection();
}

function updateTrialsSelection()
{
    trialSelectionString  = '<select name="trial" id="trials" onchange="trialSelected()" class="form-control form-control-sm">';
    trialSelectionString += '  <option value="default">Upload a Trial...</option>';

    trial_list.forEach (trial => {
        addIt = true;
        if (trialFilter['text'].length > 0 && trial.toLowerCase().indexOf(trialFilter['text'].toLowerCase()) == -1)
            addIt = false;

        if (trialFilter['hsrOnly'] && !trial.startsWith('HSRData'))
            addIt = false;

        if (trialFilter['trialsOnly'] && trial.indexOf('Trial-T0') == -1)
            addIt = false;
        
        if (addIt) {
            if (selectedTrial.localeCompare(trial) == 0)
                trialSelectionString += '  <option selected="selected" value="' + trial + '">' + trial + '</option>'
            else
                trialSelectionString += '  <option value="' + trial + '">' + trial + '</option>'
        }
    });

    trialSelectionString += '</select>'

    $('#trial_selection').html(trialSelectionString);
}

function regenerateTrial()
{
    selectedTrial = document.getElementById("trials").value;

    if (selectedTrial.localeCompare('default') == 0) {
        $("#dropzone_container").show();
        $("#delete_selected").hide();
        return;
    }

    $("#trial_load_progress").show();
    $("#dropzone_container").hide();
    $("#asr").html("<br>");

    state_info['index'] = -1;
    timings = [];
    map_width = 100;
    map_height = 100;
    svg = '';
    generateSVG();
    var items = new vis.DataSet([]);
    var groups = new vis.DataSet([
        {id: 1, order: 1, content: 'Red'},
        {id: 2, order: 2, content: 'Green'},
        {id: 3, order: 3, content: 'Blue'},
        {id: 0, order: 99, content: 'Score'}
    ]);
    timeline.setGroups(groups);
    timeline.setItems(items);

    init = {};
    init['trial'] = selectedTrial;
    init['regenerate'] = true;
    socket.emit('get_trial_data', init, namespace='/ihmc_replay_visualizer');
}

function trialSelected()
{
    selectedTrial = document.getElementById("trials").value;

    if (selectedTrial.localeCompare('default') == 0) {
        $("#dropzone_container").show();
        $("#delete_selected").hide();
        return;
    }

    $("#trial_load_progress").show();
    $("#dropzone_container").hide();
    $("#asr").html("<br>");

    state_info['index'] = -1;
    timings = [];
    map_width = 100;
    map_height = 100;
    svg = '';
    generateSVG();
    var items = new vis.DataSet([]);
    var groups = new vis.DataSet([
        {id: 1, order: 1, content: 'Red'},
        {id: 2, order: 2, content: 'Green'},
        {id: 3, order: 3, content: 'Blue'},
        {id: 0, order: 99, content: 'Score'}
    ]);
    timeline.setGroups(groups);
    timeline.setItems(items);

    init = {};
    init['trial'] = selectedTrial;
    socket.emit('get_trial_data', init, namespace='/ihmc_replay_visualizer');
}

function sio_timeline_types(msg)
{
    //console.log ("Received Timeline Types:");
    //console.log (msg);
    // Need to remove the CSS Styles added previously
    for (key in addedCSS)
    {
        document.getElementsByTagName('head')[0].removeChild(addedCSS[key]);
    }
    addedCSS = {};
    
    // New timeline types!! update the types selection list.
    if (msg['defaults'] == null) {
        cssTypes = {
            'defaults': [],
            'added': [
                {'name': 'Teaming', 'background-color': 'rgb(0, 200, 200)', 'border-color': 'rgb(0,0,0)', 'font-color': 'rgb(0,0,0)', 'z-index': '10'},
                {'name': 'Solo', 'background-color': 'rgb(110, 60, 175)', 'border-color': 'rgb(55,55,55)', 'font-color': 'rgb(255,255,255)', 'z-index': '10'},
                {'name': 'Lost', 'background-color': 'rgb(200, 200, 0)', 'border-color': 'rgb(0,0,0)', 'font-color': 'rgb(0,0,0)', 'z-index': '10'}
            ]
        };
    }
    else {
        cssTypes = msg;
    }

    // Now add the new CSS Styles to the document head.
    for (css of cssTypes['defaults']) {
        createClassForTimeLineItemFromCSS(css);
    }
    for (css of cssTypes['added']) {
        createClassForTimeLineItemFromCSS(css);
    }

    //console.log(cssTypes);

    // Finally update the selection list.
    updateCSSselection();
}

function sio_trial_data(msg)
{
    $('#mission_name').html('<small>' + msg['name'] + '</small>');
    static_root = msg['video_url'].split("videos")[0]
    videojs_player.src(msg['video_url']);
    video_start_time = 0; // 34*60+13;
    state_info['current_time'] = 0;
    state_info['index'] = 0;
    player_to_select = null;
    mission_paused = false;
    svg = msg['svg'];
    map_width = msg['width'];
    map_height = msg['height'];
    xtrans = msg['xtrans'];
    ztrans = msg['ztrans'];
    timings = msg['timings'];
    timelineData = msg['timeline'];
    eventIds = [];
    
    // Update the times and ids (if not set)
    if (timelineData['Items']) {
        for (item of timelineData['Items']) {
            if ('event-id' in item && !eventIds.includes(item['event-id']))
                eventIds.push(item['event-id'])
            if ('start' in item)
                item['start'] += diffTime;
            if ('end' in item)
                item['end'] += diffTime;
            if (!('id' in item)) {
                item['id'] = uuidv4();
            }
        }
    }
    if (timelineData['Annotations']) {
        // If this is a list, move it to be the 'Teamwork' group and update all groups to be dict's
        if (Array.isArray(timelineData['Annotations'])) {
            timelineData['Annotations'] = {'Teamwork': timelineData['Annotations']}
            updatedGroups.push('Teamwork');
            $("#save-timeline").prop('disabled', false);
            console.log(timelineData['Annotations'])
        }
        for (group in timelineData['Annotations']) {
            asDict = {};
            for (item of timelineData['Annotations'][group]) {
                if ('event-id' in item && !eventIds.includes(item['event-id']))
                    eventIds.push(item['event-id'])
                if ('start' in item) {
                    item['start'] += diffTime;
                }
                if ('end' in item)
                    item['end'] += diffTime;
                if (!('id' in item)) {
                    item['id'] = uuidv4();
                }
                asDict[item['id']] = item;
            }
            timelineData['Annotations'][group] = asDict;
        }
    }
    eventIds = eventIds.sort();

    generateTimelineInfo()
    generateSVG()

    timeline.setCustomTime(0+diffTime, 1)

    // console.log(timings);
    // console.log(timings)
    // console.log(svg)
  
    state_info['index'] = 0;
    // mission = trial_data['mission'];
    // if (mission['start_time'] != null && mission['end_time'] != null) {
    //     mission_duration = mission['end_time'] - mission['start_time'];
    //     videojs_player.duration(mission_duration)
    // }

    // toggleShowVictims();
    // toggleShowBlockages();

    // let player_list = [];

    // for (id in state_info['players'])
    //     player_list.push(state_info['players'][id]['name']);

    videojs_player.currentTime(0.001);
    $("#trial_load_progress").hide();
    $("#delete_selected").show();
}

function updateCSSselection()
{
    cssSelectionString = '<select name="css" id="css" class="form-control form-control-sm" style="width:200px;" onchange="cssSelected()">';
    cssSelectionString +=     '  <option value="default">Select A Type to Edit...</option>';
    typeNames = []
    cssTypes['added'].forEach (type => {
        typeNames.push(type['name']);
    });
    typeNames.sort().forEach (type => {
        cssSelectionString += '  <option value="' + type + '">' + type + '</option>';
    });

    cssSelectionString += '</select>'

    $('#css_selection').html(cssSelectionString);
}

async function cssSelected()
{
    selectedCSS = document.getElementById("css").value;

    if (selectedCSS == 'default')
        return;

    cssType = {};
    cssTypes['added'].forEach (type => {
        if (type['name'] == selectedCSS) {
            cssType = type;
        }
    });
    
    cssType['updated'] = false;

    // Edit Selected CSS and save changes back to the server.
    await generateEditCSSClass(cssType, false);
    updated = cssType['updated'];
    delete cssType['updated'];
    if (updated) {
        // Remove old CSS from the HTML Doc Head.
        if (addedCSS[selectedCSS]) {
            document.getElementsByTagName('head')[0].removeChild(addedCSS[selectedCSS]);
            delete addedCSS[selectedCSS]
        }

        // Add the new CSS to the HTML Doc Head.
        createClassForTimeLineItemFromCSS(cssType);
        $("#save-timeline-types").prop('disabled', false);
    }

    // Set the selected item back to default.
    document.getElementById("css").value = 'default';
}

function updateStateInfo()
{
    if (videojs_player == null)
        return;

    let currentTime = videojs_player.currentTime().toFixed(3);
    if (currentTime == state_info['current_time'])
        return;
    state_info['current_time'] = currentTime;
    currentTime *= 1000;

    asr = []

    if (state_info['index'] >= 0) {
        for (timing of timings) {
            if (timing[1] <= currentTime && (timing[2] == -1 || timing[2] >= currentTime)) {
                if (timing[3] == 'pd') {            // player path data
                    // show the path and translate/rotate the player icon.
                    pe = document.getElementById(timing[0]+'_'+timing[2]);
                    if (pe != null) {
                        for (i = 0; i < pe.children.length; i++) {
                            pe.children[i].setAttribute('display', 'block')
                        }
                    }
                    pe = document.getElementById(timing[0]);
                    if (pe != null)
                        pe.setAttribute('transform', 'translate'+timing[4]);
                    pe = document.getElementById(timing[0]+'_rot');
                    if (pe != null)
                        pe.setAttribute('transform', 'rotate'+timing[5]);
                }
                else if (timing[3] == 'gd') {            // group display
                    pe = document.getElementById(timing[0]);
                    if (pe != null) {
                        for (i = 0; i < pe.children.length; i++) {
                            pe.children[i].setAttribute('display', 'block')
                        }
                    }
                }
                else if (timing[3] == 'stroke') {   // set stroke
                    pe = document.getElementById(timing[0]);
                    if (pe != null) {
                        pe.setAttribute('stroke', timing[4]);
                    }
                }
                else if (timing[3] == 'fill') {   // set fill
                    pe = document.getElementById(timing[0]);
                    if (pe != null) {
                        pe.setAttribute('fill', timing[4]);
                    }
                }
                else if (timing[3] == 'st') {       // set text
                    $('#'+timing[0]).html('<small>' + timing[4] + '</small>')
                }
                else if (timing[3] == 'at') {       // append text
                    asr.push('<b>' + timing[4] + ' - </b>' + timing[5] + '<br>')
                }
            }
            else {
                if (timing[3] == 'pd') {
                    hideShow = timing[2] >= currentTime ? 'none' : 'block';
                    // hide the path
                    pe = document.getElementById(timing[0]+'_'+timing[2]);
                    for (i = 0; i < pe.children.length; i++) {
                        pe.children[i].setAttribute('display', hideShow)
                    }
                }
                else if (timing[3] == 'gd') {            // group display
                    pe = document.getElementById(timing[0]);
                    if (pe != null) {
                        for (i = 0; i < pe.children.length; i++) {
                            pe.children[i].setAttribute('display', 'none')
                        }
                    }
                }
            }
        } 
    }

    asrhtml = "<br>";
    if (asr.length > 0) {
        asrhtml = "";
        for (i = 0; i < asr.length && i < 6; i++) {
            asrhtml += asr[asr.length-1-i]
        }
    }
    $('#asr').html(asrhtml);

    if (!timelineMovingTime)
        timeline.setCustomTime(currentTime+diffTime, 1)

    $('#mission_timer').html('<small>' + state_info['current_time'] + '</small>');
}

function rescaleMap()
{
    let w = map_width;
    let h = map_height;

    let trans_x = 0;
    let trans_y = 0;
    let rotate = 0;

    if (rotate90) {
        let tmp = w;
        w = h;
        h = tmp;
        rotate = 270;
        trans_x = -(map_width*scale);
    }
              
    let svg_map = document.getElementById('svg_map');
    if (svg_map != null) {
        svg_map.setAttribute('height', h*scale);
        svg_map.setAttribute('width', w*scale);

        svg_map_transform = document.getElementById('svg_map_transform');
        if (svg_map_transform != null) 
            svg_map_transform.setAttribute('transform', 'rotate('+rotate+') translate('+trans_x+','+trans_y+') scale('+scale+')');
    }
}

function zoomIn()
{
    if (scale >= 1.0) {
        scale += 0.5;
    }
    else {
        scale += 0.1;
    }
    rescaleMap();
}

function zoomOut()
{
    if (scale <= 1) {
        scale -= 0.1;
    }
    else {
        scale -= 0.5;
    }
    rescaleMap();
}

function toggleShowVictims()
{
    document.getElementById('svg_victims').setAttribute('display', document.getElementById('show_victims').checked ? 'block' : 'none');
}

function toggleShowBlockages()
{
    document.getElementById('svg_rubble').setAttribute('display', document.getElementById('show_blockages').checked ? 'block' : 'none');
}

function toggleShowPathRed()
{
    document.getElementById('svg_p_R_path').setAttribute('display', document.getElementById('show_path_red').checked ? 'block' : 'none');
}

function toggleShowPathGreen()
{
    document.getElementById('svg_p_G_path').setAttribute('display', document.getElementById('show_path_green').checked ? 'block' : 'none');
}

function toggleShowPathBlue()
{
    document.getElementById('svg_p_B_path').setAttribute('display', document.getElementById('show_path_blue').checked ? 'block' : 'none');
}

function toggleShowMarkers()
{
    document.getElementById('svg_markers').setAttribute('display', document.getElementById('show_markers').checked ? 'block' : 'none');
}

function deleteSelected()
{
    if (selectedTrial == 'default')
        return;

    if (confirm("Are you sure you want to delete the Trial info for: " + selectedTrial)) {
        socket.emit('delete_trial_data', {"trial":selectedTrial}, namespace='/ihmc_replay_visualizer');

        videojs_player.src('static/videos/30minutes.mp4');
        semanticMap = '';
        semanticMapWithUpdates = null;
        baseMap = '';
        baseMapWithBlockages = '';
        path_svg_ids = [];
        trial_data = {};
        state_info = {'index': -1, 'current_time': 0}
        video_start_time = 0;
        selectedTrial = 'default';
        map_width = 100;
        map_height = 100;
        svg = '';
        generateSVG();
    }
}

function generateSVG()
{
    // Generate the basemap SVG which computes the map bounds
    let w = map_width;
    let h = map_height;

    let trans_x = 0;
    let trans_y = 0;
    let rotate = 0;

    if (rotate90) {
        let tmp = w;
        w = h;
        h = tmp;
        rotate = 270;
        trans_x = -(map_width*scale);
    }

    // create the SVG with all components
    svg_map = '<svg id="svg_map" height="' + (h*scale) + '" width="' + (w*scale) + '" xmlns="http://www.w3.org/2000/svg"' + 
                ' onmouseup="svgMouseUp(evt)"' + 
                ' >' +
                '<g id="svg_map_transform" transform="rotate('+rotate+') translate('+trans_x+',' + trans_y+') scale(' + scale + ')">' +
                  '<rect x="' + 0 + '" y="' + 0 + '" width="' + map_width + '" height="' + map_height + '" style="fill:rgb(255,255,255)"></rect>' +
                  svg + 
                '</g>' +
              '</svg>';

    // console.log(svg_map);

    $('#div_map').html(svg_map);
}

function svgMouseUp(evt)
{
    let e = document.getElementById('svg_map');
    let dim = e.getBoundingClientRect();
    let x = ((evt.clientX - dim.left) / scale);
    let y = ((evt.clientY - dim.top) / scale);
    if (rotate90) {
        let tmp = x;
        x = map_width - y;
        y = tmp;
    }
    x += xtrans;
    y += ztrans;
    
    console.log("SVG Mouse UP at: " + Math.floor(x) + ", " + Math.floor(y) + " - which:" + evt.which + " shift:" + evt.shiftKey + " alt:" + evt.altKey + " meta:" + evt.metaKey);
}

function createClassForTimeLineItemFromCSS(css) {
    className = css['name'];
    height = css['height'] ? css['height'] : null;
    z_index = css['z-index'] ? css['z-index'] : null;
    background_color = css['background-color'] ? css['background-color'] : null;
    border_color = css['border-color'] ? css['border-color'] : null;
    border_width = css['border-width'] ? css['border-width'] : null;
    border_radius = css['border-radius'] ? css['border-radius'] : null;
    border_style = css['border-style'] ? css['border-style'] : null;
    font_color = css['font-color'] ? css['font-color'] : null;
    font_family = css['font-family'] ? css['font-family'] : null;
    font_size = css['font-size'] ? css['font-size'] : null;
    dot_radius = css['dot-radius'] ? css['dot-radius'] : null;
    dot_width = css['dot-width'] ? css['dot-width'] : null;
    dot_color = css['dot-color'] ? css['dot-color'] : null;
    dot_style = css['dot-style'] ? css['dot-style'] : null;
    line_width = css['line-width'] ? css['line-width'] : null;
    line_color = css['line-color'] ? css['line-color'] : null;
    line_style = css['line-style'] ? css['line-style'] : null;

    createClassForTimelineItem(className, height, z_index, background_color, border_color, border_width, border_radius, border_style, 
                               font_color, font_family, font_size, dot_radius, dot_width, dot_color, dot_style,
                               line_width, line_color, line_style);
}

function createClassForTimelineItem(class_name, height=null, z_index=null,                              // '.???', '??', '1'
                                    background_color=null, border_color=null,                           // '#d5ddf6', '#97b0f8'
                                    border_width=null, border_radius=null, border_style=null,           // '1px', '2px', 'solid'
                                    font_color=null, font_family=null, font_size=null,                  // '#1a1a1a', '??', '??'
                                    dot_radius=null, dot_width=null, dot_color=null, dot_style=null,    // '4px', '4px', '<border_color>', 'solid' 
                                    line_width=null, line_color=null, line_style=null)                  // '1px', 'border_color', 'solid'
{
    rules = '';
    main = '';
    dot = '';
    line = '';
    box = '';
    range = '';

    // setup the main item class features
    if (border_color !== null) {
        main += 'border-color: ' + border_color + ';';
        box += 'border-color: ' + border_color + ';';
        range += 'border-color: ' + border_color + ';';
    }
    if (border_width !== null) {
        main += 'border-width: ' + border_width + ';';
        box += 'border-width: ' + border_width + ';';
        range += 'border-width: ' + border_width + ';';
    }
    if (border_radius !== null) {
        // main += 'border-radius: ' + border_radius + ';';
        box += 'border-radius: ' + border_radius + ';';
        range += 'border-radius: ' + border_radius + ';';
    }
    if (border_style !== null) {
        main += 'border-style: ' + border_style + ';';
        box += 'border-style: ' + border_style + ';';
        range += 'border-style: ' + border_style + ';';
    }
    if (background_color !== null) {
        main += 'background-color: ' + background_color + ';';
        box += 'background-color: ' + background_color + ';';
        range += 'background-color: ' + background_color + ';';
    }
    if (z_index !== null)
        main += 'z-index: ' + z_index + ';';

    if (font_color !== null)
        main += 'color: ' + font_color + ';';
    if (font_family !== null)
        main += 'font-family: ' + font_family + ';';
    if (font_size !== null)
        main += 'font-size: ' + font_size + ';';

    // setup the dot class features
    if (dot_radius !== null) 
        dot += 'border-radius: ' + dot_radius + ';';
    if (dot_width !== null)
        dot += 'border-width: ' + dot_width + ';';
    if (dot_color !== null) 
        dot += 'border-color: ' + dot_color + ';';
    if (dot_style !== null)
        dot += 'border-style: ' + dot_style + ';';

    // setup the line class features
    if (line_width !== null)
        line += 'border-left-width: ' + line_width + ';';
    if (line_color !== null)
        line += 'border-left-color: ' + line_color + ';';
    if (line_style !== null)
        line += 'border-left-style: ' + line_style + ';';

    // setup the box class features
    if (height !== null) {
        box += 'height: ' + height + ';';
        range += 'height: ' + height + ';';
    }

    //console.log("createCSS for '" + class_name + "' : " + main.length + ", " + dot.length + ", " + line.length + ", " + box.length + ", " + range.length)

    names = []
    rules = []
    if (main.length > 0) {names.push('.vis-item.' + class_name); rules.push(main);}
    if (dot.length > 0) {names.push('.vis-item.vis-dot.' + class_name); rules.push(dot);}
    if (line.length > 0) {names.push('.vis-item.vis-line.' + class_name); rules.push(line);}
    if (box.length > 0) {names.push('.vis-item.vis-box.' + class_name); rules.push(box);}
    if (range.length > 0) {names.push('.vis-item.vis-range.' + class_name); rules.push(range);}

    if (names.length > 0)
        createClass(class_name, names, rules);
}

function createClass(class_name, names,rules){
    // createClass(['.whatever'],["background-color: green;"]);
    if (addedCSS[class_name]) {
        document.getElementsByTagName('head')[0].removeChild(addedCSS[class_name]);
        delete addedCSS[class_name];
    }

    var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    for (let i = 0; i < names.length; i++) {
        if(!(style.sheet||{}).insertRule) 
            (style.styleSheet || style.sheet).addRule(names[i], rules[i]);
        else
            style.sheet.insertRule(names[i]+"{"+rules[i]+"}",0);
    }
    addedCSS[class_name] = style;
}

function generateTimelineInfo()
{
    // Now set the Groups
    timeline.setGroups(new vis.DataSet(timelineData['Groups']));
    setTimelineItems();
    updateGroupSelection();
}

function setTimelineItems()
{
    timelineItems = new vis.DataSet();

    if (timelineData['Items']) {
        for (item of timelineData['Items'])
            if (!('className' in item) || !filters['types'].includes(item['className']))
                if (item['group'] == 0 || (item['type'] && item['type'] == 'background'))
                    timelineItems.add(item);
    
        if (!filters['groups'].includes('Autogenerated'))
            for (item of timelineData['Items'])
                if (!('className' in item) || !filters['types'].includes(item['className']))
                    if (item['group'] != 0 && (!item['type'] || item['type'] != 'background'))
                            timelineItems.add(item);
    }
    if (timelineData['Annotations']) {
        for (group in timelineData['Annotations']) {
            if (filters['groups'].includes(group))
                continue;
            for (key in timelineData['Annotations'][group])
                if (!('className' in timelineData['Annotations'][group][key]) || !filters['types'].includes(timelineData['Annotations'][group][key]['className'])) {
                    timelineItems.add(timelineData['Annotations'][group][key]);
                }
        }
    }
    timeline.setItems(timelineItems);
}

function updateGroupSelection(selected)
{
    groupSelectionString = '<select name="group" id="group" class="form-control form-control-sm" style="width:200px;" onchange="groupSelected()">';
    groupNames = []
    if (!selected || selected == undefined || selected == null)
        selected = document.getElementById("group").value;
    if (timelineData['Annotations']) {
        for (group in timelineData['Annotations']) {
            groupNames.push(group);
        }
        groupNames.sort().forEach (group => {
            hidden = filters['groups'].includes(group) ? " (Hidden)" : "";
            if (selected && group == selected)
                groupSelectionString += '  <option selected value="' + group + '">' + group + hidden + '</option>';
            else
                groupSelectionString += '  <option value="' + group + '">' + group + hidden + '</option>';
        });
    }
    if (groupNames.length <= 0) {
        hidden = filters['groups'].includes("Teamwork") ? " (Hidden)" : "";
        groupSelectionString += '  <option value="Teamwork">Teamwork' + hidden + '</option>';
    }
    groupSelectionString += '</select>'

    $('#group_selection').html(groupSelectionString);
    selectedGroup = document.getElementById("group").value;
}

function groupSelected()
{
    selectedGroup = document.getElementById("group").value;
    setTimelineItems()
}

async function generateTimelineGroupName(groupName)
{
    generateNameTitle = "New Group"
    let name = '';
    if (groupName != null) {
        generateNameTitle = "Rename Group";
        name = groupName;
    }

    const { value: formValues } = await Swal.fire({
        title: generateNameTitle,
        width: '600px',
        html:
            '<div style="float: left;">' + 
                '<label style="width:100px;text-align:right;">Name:</label>' +
                '<input id="swal-name" class="swal2-input" placeholder="Group Name (Required)!!" value="' + name + '">' +
            '</div>' +
            fontDataList + colorDataList,
        focusConfirm: false,
        preConfirm: () => {
          return {
            'name': document.getElementById('swal-name').value.trim().replace(/\s/g, '_'),  
          }
        }
      })
      
    newName = null;
    if (formValues) {
        newName = formValues['name'];
        if (groupName != null && newName == groupName)
            newName = null;
        else {
            // make sure there is not already a group with this name
            if (newName in timelineData['Annotations']) {

                newName = null;
            }
        }
    }

    return newName;
}

async function newTimelineGroup()
{
    if (!('Annotations' in timelineData))
        timelineData['Annotations'] = {};
    // Open a dialog and get the new timeline group name.
    newGroupName = await generateTimelineGroupName(null);
    // If 'OK' 
    if (newGroupName != null) {
        // - add the new group to the timelineData['Annotations'] object
        timelineData['Annotations'][newGroupName] = [];
        // - updated the group selection list and make sure the new group is selected.
        updateGroupSelection(newGroupName);
        // - add the new group to the 'updatedGroups' object.
        if (!updatedGroups.includes(newGroupName))
            updatedGroups.push(newGroupName);
        $("#save-timeline").prop('disabled', false);
    }
}

function deleteTimelineGroup()
{
    // Confirm that they want to delete the group
    if (confirm("Are you sure you want to delete the group: " + selectedGroup)) {
        // If 'OK' then delete the timelineData['Annotations'] object for this group and add the group to the 'updatedGroups' object
        delete timelineData['Annotations'][selectedGroup];
        if (!updatedGroups.includes(selectedGroup))
            updatedGroups.push(selectedGroup);
        // and update the group selection list and make the first item the selected group.
        updateGroupSelection();
        setTimelineItems();
        $("#save-timeline").prop('disabled', false);
    }
}

async function renameTimelineGroup()
{
    oldGroupName = selectedGroup;
    // Open a dialog and get the new timeline group name (show the old one on open).
    newGroupName = await generateTimelineGroupName(selectedGroup);
    // Make sure it does not conflict with an existing group name and is a valid JSON Object key
    // If 'OK':
    if (newGroupName != null) {
        // - add the new group to the timelineData['Annotations'] object
        // - move all items in the old group to the new group
        timelineData['Annotations'][newGroupName] = timelineData['Annotations'][selectedGroup];
        // - remove the old group from the timelineData['Annotations'] object
        delete timelineData['Annotations'][selectedGroup];
        // - updated the group selection list and make sure the renamed group is selected.
        if (!updatedGroups.includes(selectedGroup))
            updatedGroups.push(selectedGroup);
        if (!updatedGroups.includes(newGroupName))
            updatedGroups.push(newGroupName);
        if (filters['groups'].includes(selectedGroup)) {
            filters['groups'].push(newGroupName);
            index = filters['groups'].indexOf(selectedGroup);
            filters['groups'].splice(index, 1);
            // filters['groups'].pop(selectedGroup);
            console.log(filters);
        }
        // - add the new group and the old group to the 'updatedGroups' object.
        updateGroupSelection();
        $("#save-timeline").prop('disabled', false);
    }
}

function clearTimelineGroup()
{
    // Confirm that they want to remove all items from this timeline group
    if (confirm("Are you sure you want to delete all items in the group: " + selectedGroup)) {
        // If 'OK' then remove all items from the timelineData['Annotations'][group], update the timeline, and add the group to the 'updatedGroups' object.
        timelineData['Annotations'][selectedGroup] = [];
        if (!updatedGroups.includes(selectedGroup))
            updatedGroups.push(selectedGroup);
        $("#save-timeline").prop('disabled', false);
        setTimelineItems();
    }
}

function moveToTimelineGroup()
{
    selected = timeline.getSelection();
    if (selected.length <= 0)
        return;

    if (!('Annotations' in timelineData))
        return;

    // For each timeline item selected
    for (group in timelineData['Annotations']) {
        if (group == selectedGroup)
            continue;

        for (id in timelineData['Annotations'][group]) {
            if (selected.includes(id)) {
                // move it to the currently selected group and remove it from its previous group.
                timelineData['Annotations'][selectedGroup][id] = timelineData['Annotations'][group][id]
                delete timelineData['Annotations'][group][id];
                // Record each updated group in the 'updatedGroups' object
                if (!updatedGroups.includes(group)) {
                    updatedGroups.push(group);
                    $("#save-timeline").prop('disabled', false);
                }
                if (!updatedGroups.includes(selectedGroup)) {
                    updatedGroups.push(selectedGroup);
                    $("#save-timeline").prop('disabled', false);
                }
            }
        }
    }
}

async function openFilterTimelineDialog()
{
    htmlText = "Checked items are visible, unchecked items will be hidden.";
    htmlText +=  '<div class="faq">' +
                '  <details>' +
                '    <summary>Default Types</summary>' +
                '    <div>';
    defaults = []
    for (css in timelineData['CSS']['defaults'])
        defaults.push(timelineData['CSS']['defaults'][css]['name']);
    defaults = defaults.sort();
    for (t of defaults) {
        filtered = !filters['types'].includes(t) ? 'checked' : '';
        htmlText += '    <input id="swal-css-' + t + '" class="swal2-checkbox" type="checkbox" ' + filtered + '>' +
                    '    <label style="width:350px;text-align:left;">' + t + '</label><br>';
    }
            
    htmlText += '    </div>' +
                '  </details>';

    htmlText += '  <details>' +
                '    <summary>Added Types</summary>' +
                '    <div>';
    added = []
    for (css in timelineData['CSS']['added'])
        added.push(timelineData['CSS']['added'][css]['name']);
    added = added.sort();
    for (t of added) {
        filtered = !filters['types'].includes(t) ? 'checked' : '';
        htmlText += '    <input id="swal-css-' + t + '" class="swal2-checkbox" type="checkbox" ' + filtered + '>' +
                    '    <label style="width:350px;text-align:left;">' + t + '</label><br>';
    }
    htmlText += '    </div>' +
                '  </details>';

    htmlText += '  <details>' +
                '    <summary>Groups</summary>' +
                '    <div>';
    groups = []
    if ('Annotations' in timelineData)
        for (group in timelineData['Annotations'])
            groups.push(group)
    groups = groups.sort();
    groups.unshift('Autogenerated')
    for (g of groups) {
        filtered = !filters['groups'].includes(g) ? 'checked' : '';
        htmlText += '    <input id="swal-grp-' + g + '" class="swal2-checkbox" type="checkbox" ' + filtered + '>' +
                    '    <label style="width:350px;text-align:left;">' + g + '</label><br>';
    }
    htmlText += '    </div>' +
                '  </details>' +
                '</div>';

    const { value: formValues } = await Swal.fire({
        title: 'Timeline Filters',
        width: '600px',
        html: htmlText,
        focusConfirm: false,
        preConfirm: () => {
            ret = {'types': [], 'groups': []}
            for (t of defaults)
                if (!document.getElementById('swal-css-'+t).checked)
                    ret['types'].push(t); 
            for (t of added)
                if (!document.getElementById('swal-css-'+t).checked)
                    ret['types'].push(t); 
            for (g of groups)
                if (!document.getElementById('swal-grp-'+g).checked)
                    ret['groups'].push(g); 
            return ret;
        }
      })
      
    if (formValues) {
        filters = ret;
        localStorage.setItem('vis_filters', JSON.stringify(filters));
        setTimelineItems()
        updateGroupSelection()
    }
}