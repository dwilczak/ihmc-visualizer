import json
from .JAGHelper import JointActivityHelper

sg1_order = 100
sg0_order = 200
sgc_order = 300
sg2_order = 0

activity_style_map = {
        'check-if-unlocked':     'background-color: rgba(255, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;',
        'unlock-victim':         'background-color: rgba(  0, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;',
        'pick-up-victim':        'background-color: rgba(  0, 255, 255, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;',
        'drop-off-victim':       'background-color: rgba(  0,   0, 255, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;',
        'at-proper-triage-area': 'background-color: rgba(255, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;',
        'default':               'background-color: rgba(255,   0,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;'
    }

def generateDefaultTimelineCSS():
    css = []

    marker_colors = {
        # Markers ranked from most used to least used.
        "marker_novictim": "black",
        "marker_criticalvictim": "red",
        "marker_regularvictim": "green",
        "marker_bonedamage": "orange",
        "marker_threat": "brown",
        "marker_sos": "purple",
        "marker_abrasion": "pink",
        "marker_rubble": "grey"
    }

    # Adding styles for each marker.
    for name,color in marker_colors.items():
        css.append(
            {
                "z-index": "2",
                "border-width": "4px",
                "border-radius": "2px",
                "name": name,
                "border-color": color,
                "dot-color": color
            }
        )

    css.append({'name':'def_rubble', 
                'z-index':'7', 
                'dot-radius':'3px', 'dot-width':'3px', 'dot-color':'rgb(100, 100, 100, 0.5)',
                'line-width':'1px', 'line-color':'rgb(50, 50, 50)'
                })
    css.append({'name':'def_triaged-victim', 
                'z-index':'4', 'background-color':'rgba(0, 255, 0, 0.5)',
                'border-color':'rgba(230, 255, 15, 0.5)', 'border-width':'3px', 'border-radius':'3px',
                'dot-radius':'1px', 'dot-width':'4px', 'dot-color':'rgba(0, 255, 0, 0.5)',
                'line-width':'1px', 'line-color':'rgba(0, 255, 0, 0.5)'
               })
    css.append({'name':'def_triaged-critical-victim', 
                'z-index':'4', 'background-color':'rgba(255, 0, 0, 0.5)',
                'border-color':'rgba(230, 255, 15, 0.5)', 'border-width':'3px', 'border-radius':'3px',
                'dot-radius':'1px', 'dot-width':'4px', 'dot-color':'rgba(255, 0, 0, 0.5)',
                'line-width':'1px', 'line-color':'rgba(255, 0, 0, 0.5)'
               })
    css.append({'name':'def_carrying-victim', 
                'z-index':'2', 'height':'20px', 'background-color':'rgba(200, 150, 0, 0.5)', 
                'border-color':'rgba(0, 0, 0, 0.5)', 'border-width':'1px', 'border-radius':'2px'})
    css.append({'name':'def_frozen', 
                'z-index':'3', 'height': '20px', 'background-color':'rgb(150, 255, 255)', 
                'border-color':'rgb(255, 0, 0)', 'border-width':'1px', 'border-radius':'0px'})
    css.append({'name':'def_triaging-success', 
                'z-index':'5', 'height':'8px', 'background-color':'rgb(140, 185, 15)', 
                'border-color':'rgb(140, 185, 15)', 'border-width':'1px', 'border-radius':'2px'})
    css.append({'name':'def_triaging-fail', 
                'z-index':'5', 'height':'8px', 'background-color':'rgb(140, 185, 15)', 
                'border-color':'rgb(255, 0, 0)', 'border-width':'1px', 'border-radius':'2px'})
    css.append({'name':'def_unlocked-victim', 
                'z-index':'11', 
                'dot-radius':'3px', 'dot-width':'3px', 'dot-color':'rgb(255, 0, 0)'
                })
    css.append({'name':'def_score', 'height':'5px', 'z-index':'10', 'background-color':'rgb(255, 255, 200)', 
                'font-size':'8pt', 'font-family':'Arial', 'dot-radius':'0px', 'dot-width':'1px'})

    css.append({'name':'def_blank', 
                'z-index':'0', 'height':'1px', 'background-color':'rgba(255, 255, 255, 0.0)',
                'border-color':'rgba(255, 255, 255, 0.0)', 'border-width':'0px', 'border-radius':'0px',
                'dot-radius':'0px', 'dot-width':'0px', 'dot-color':'rgba(255, 255, 255, 0.0)',
                'line-width':'0px', 'line-color':'rgba(255, 255, 255, 0.0)'
               })

    css.append({'name':'def_jag_activity', 'padding-top': '2px', 'font-size': '8px',
                'z-index':'2', 'height':'30px', 'background-color':'rgba(150, 255, 255, 0.5)', 
                'border-color':'rgb(0, 0, 0)', 'border-width':'1px'})
    css.append({'name':'def_jag_preparing', 
                'z-index':'3', 'height':'10px', 'background-color':'rgba(200, 150, 0, 0.5)', 
                'border-color':'rgb(0, 0, 0)', 'border-width':'1px'})
    css.append({'name':'def_jag_addressing', 
                'z-index':'4', 'height':'10px', 'background-color':'rgba(150, 200, 0, 0.5)', 
                'border-color':'rgb(0, 0, 0)', 'border-width':'1px'})

    types = [] 
    types.append({'name': 'Teaming', 'background-color': 'rgb(0, 200, 200)', 'border-color': 'rgb(0,0,0)', 'font-color': 'rgb(0,0,0)', 'z-index': '10'})
    types.append({'name': 'Solo', 'background-color': 'rgb(110, 60, 175)', 'border-color': 'rgb(55,55,55)', 'font-color': 'rgb(255,255,255)', 'z-index': '10'})
    types.append({'name': 'Lost', 'background-color': 'rgb(200, 200, 0)', 'border-color': 'rgb(0,0,0)', 'font-color': 'rgb(0,0,0)', 'z-index': '10'})

    return {'defaults': css, 'added': types}

def getChildLeaves(tree, leaves=[]):

    if 'children' in tree and len(tree['children']) > 0:
        for child in tree['children']:
            getChildLeaves(child, leaves)
    else:
        leaves.append(tree)

    return leaves

def generateTimelineData(players, timeline, basemap, rubble, victims, expType, maxEndTime, jah, planning_stop_time,markers):
    timelineData = {}
    timelineData['CSS'] = generateDefaultTimelineCSS()

    timelineData['Groups'] = [{'id': 0, 'order': 99, 'content': 'Score', 'subgroupStack': {'sg1': True, 'sg0': False}}]
    timelineData['Items'] = []
    # timelineData['Items'].append({'start': -20, 'end': 1200000, 'group': 0, 'subgroup': 'sg1', 'type': 'background', 'style': 'background-color: rgba(255,255,255,0.0); height: 90px;'})

    if timeline['endTime'] > 0:
        style = 'color: black; background-color: gray; padding-top: 0px; font-size: 10px;'
        timelineData['Items'].append({'start': timeline['endTime'], 'end': timeline['endTime']+60000, 'content': 'End', 'style': style, 'type': 'background', 'group': 0})

    for item in timeline['unlocked']:
        timelineData['Items'].append({'start': item['st'], 'title': 'Unlocked cv-' + str(item['vid']), 'group': 0, 'type': 'point', 'editable': False, 'className': 'def_unlocked-victim', 'subgroup': 'sg0', 'subgroupOrder': sg0_order})

    for item in timeline['score']:
        timelineData['Items'].append({'start': item['st'], 'content': str(item['score']), 'group': 0, 'type': 'point', 'editable': False, 'className': 'def_score', 'subgroup': 'sg0', 'subgroupOrder': sg0_order})

    if 'perturbation' in timeline:
        style = 'color: black; background-color: pink; padding-top: 0px; font-size: 10px;'
        for item in timeline['perturbation']:
            if item['type'] == 'blackout':
                timelineData['Items'].append({'start': item['start'], 'end': item['end'], 'content': 'Comms Black Out', 'style': style, 'type': 'background', 'group': 0})
            elif item['type'] == 'rubble':
                timelineData['Items'].append({'start': item['start'], 'end': item['end'], 'content': 'Rubble', 'style': style, 'type': 'background', 'group': 0})

    # process the merged jag messages:
    # team = jah.get_player_by_callsign('black')
    # if team is not None:
    #     jags = team.joint_activity_model.jag_instances

    #     for jag in jags:
    #         print("Handling a JAG: " + jag.short_string())
    #         processJag(jag, players)
    # else: 
    #     print("**** No Team JAGS!!!!! ****")

    tansparency = 0.8
    max = 255.0 # was 200.0
    min = 150.0  # was 100.0
    colorRange = max-min

    # Add the markers to the timline data logic and file.
    groups = {"Red": 1, "Green": 2, "Blue": 3}

    for marker in markers:

        timelineData["Items"].append(
            {
                "start": markers[marker][0]["start"],
                "title": markers[marker][0]["block_type"].split("_")[1],
                "group": groups[markers[marker][0]["callsign"]],
                "subgroup": "sg0",
                "subgroupOrder": 200,
                "type": "point",
                "editable": False,
                "className": 'marker_'+markers[marker][0]["block_type"].split("_")[1],
                "marker_location": f"{markers[marker][0]['z']}{markers[marker][0]['x']}",
            }
        )

    for key in players.keys():
        player = players[key]
        if expType == 3:
            timelineData['Groups'].append({'id': player['id'], 'order': player['id'], 
                                        'content': player['callsign'], 
                                        'title': 'Id: ' + player['participant_id'],
                                        'style': 'color: white; background-color: ' + player['callsign'] + ';',
                                        'subgroupStack': {'sg1': True, 'sg0': False, 'sgc': False},
                                        'subgroupOrder': 'subgroupOrder',
                                        'callsign': player['callsign'],
                                        'markerblocklegend': None,
                                        'staticmapversion': None})

            # timelineData['Items'].append({'start': 0, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 1, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2.1', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 2, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2.1', 'editable': False, 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0, height:5px'})
            # timelineData['Items'].append({'start': 3, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg1', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 4, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg0', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 5, 'end': 1200000, 'group': player['id'], 'subgroup': 'sgc', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})

            # handle jag messages
            jagP = jah.get_player_by_callsign(player['callsign'])
            jagGroups = []
            if jagP is not None:
                jags = jagP.joint_activity_model.get_known_victims()
                if jags is not None:
                    for jag in jags:
                        if jag.is_complete():
                            print("COMPLETED JAG!!")
                        print(player['callsign'] + " - Handling a JAG: " + jag.short_string())
                        processJag(jag, jagGroups, player['callsign'].lower(), player['id'], planning_stop_time)
            
            # For each jag Group, create a subgroup and then add all the jags in each jag Group to it's subgroup on the timeline.
            # print("*** There are " + str(len(jagGroups)) + " Jag Groups")
            jagGroupIndex = 2
            for jg in jagGroups:
                subGrp = 'sg' + str(jagGroupIndex)
                timelineData['Groups'][-1]['subgroupStack'][subGrp] = False
                for ja in jg:
                    for item in ja['items']: 
                        item['subgroup'] = subGrp
                        item['subgroupOrder'] = jagGroupIndex
                        timelineData['Items'].append(item)
                for ja in jg:
                    if ja['main'] is not None:
                        ja['main']['subgroup'] = subGrp
                        ja['main']['subgroupOrder'] = jagGroupIndex
                        if ja['main']['start'] == ja['main']['end']:
                            ja['main']['end'] += 100
                        timelineData['Items'].append(ja['main'])

                jagGroupIndex += 1

            print(timelineData['Groups'][-1]['subgroupStack'])
            

            # Hard coding the planning stage background to be 2:03 long.
            toolLevelColor = "rgba(120, 120, 120, " + str(tansparency) + ")"
            role_name = "UNKNOWN"
            if len(player['role_changes']) > 0:
                rc = player['role_changes'][-1]
                rc['level'] = 100
                role_name = rc['role']
                if rc['role'].lower() == 'transporter':
                    percentColor = str(round(colorRange * (100.0 - rc['level'])/100.0 + min))
                    # toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')' 
                    toolLevelColor = 'rgba(255, ' + percentColor + ', ' + '0' + ', ' + str(tansparency) + ')' 
                elif rc['role'].lower() == 'medic':
                    percentColor = round(colorRange * (100.0 - rc['level'])/100.0 + min)
                    # toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', ' + str(tansparency) + ')' 
                    # toolLevelColor = 'rgba(' + '0' + ', 255, ' + percentColor + ', ' + str(tansparency) + ')' 
                    toolLevelColor = 'rgba(' + str(percentColor) + ', ' + str(round(percentColor/2)) + ', 255, ' + str(tansparency) + ')' 
                elif rc['role'].lower() == 'rubbler':
                    percentColor = str(round(colorRange * (100.0 - rc['level'])/100.0 + min - 25))
                    # toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, ' + str(tansparency) + ')'
                    toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')'

            toolLevelColor = 'background-color: ' + toolLevelColor + ';'
            role_time = planning_stop_time if planning_stop_time > 60000 else 60000
            timelineData['Items'].append({'start': -10, 'end': role_time, 'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order,
                                          'content': role_name, 'type': 'background', 'style': toolLevelColor})
        elif expType == 2:
            mbLegend = player['markerblocklegend'] if 'markerblocklegend' in player.keys() else 'None'
            if len(mbLegend) <= 0:
                mbLegend = "None"
            smv = player['staticmapversion'] if 'staticmapversion' in player.keys() else 'Saturn None'
            if len(smv) <= 0:
                smv = "Saturn None"
            timelineData['Groups'].append({'id': player['id'], 'order': player['id'], 
                                        'content': player['callsign'] + '<br>[' + mbLegend[0] + '] / ' + smv.replace('Saturn', ''), 
                                        'title': 'Id: ' + player['participant_id'] + ' Markers: ' + mbLegend + ' Map: ' + smv,
                                        'style': 'color: white; background-color: ' + player['callsign'] + ';',
                                        'subgroupStack': {'sg1': True, 'sg0': False, 'sgc': False}, 
                                        'callsign': player['callsign'],
                                        'markerblocklegend': mbLegend,
                                        'staticmapversion': smv})

            timelineData['Items'].append({'start': 0, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg1', 'subgroupOrder': sg1_order, 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            timelineData['Items'].append({'start': 0, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order, 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            timelineData['Items'].append({'start': 0, 'end': 1200000, 'group': player['id'], 'subgroup': 'sgc', 'subgroupOrder': sgc_order, 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})

            for rc in player['role_changes']:
                toolLevelColor = 'rgba(255, 255, 255, ' + str(tansparency) + ')' 
                if rc['level'] > 0.0:

                    if rc['role'].lower() == 'searcher':
                        percentColor = str(round(colorRange * (100.0 - rc['level'])/100.0 + min))
                        # toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')' 
                        toolLevelColor = 'rgba(255, ' + percentColor + ', ' + '0' + ', ' + str(tansparency) + ')' 
                    elif rc['role'].lower() == 'medic':
                        percentColor = round(colorRange * (100.0 - rc['level'])/100.0 + min)
                        # toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', ' + str(tansparency) + ')' 
                        # toolLevelColor = 'rgba(' + '0' + ', 255, ' + percentColor + ', ' + str(tansparency) + ')' 
                        toolLevelColor = 'rgba(' + str(percentColor) + ', ' + str(round(percentColor/2)) + ', 255, ' + str(tansparency) + ')' 
                    elif rc['role'].lower() == 'rubbler':
                        percentColor = str(round(colorRange * (100.0 - rc['level'])/100.0 + min - 25))
                        # toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, ' + str(tansparency) + ')'
                        toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')'
                    # percentColor = str(round(255.0 * (100.0 - rc['level'])/100.0))
                    # toolLevelColor = 'rgba(255, 255, 255, 0.5)'
                    # if rc['role'].lower() == 'searcher':
                    #     toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', 0.5)' 
                    # elif rc['role'].lower() == 'medic':
                    #     toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', 0.5)' 
                    # elif rc['role'].lower() == 'rubbler':
                    #     toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, 0.5)'

                toolLevelColor = 'background-color: ' + toolLevelColor + ';'
                if len(timelineData['Items']) > 0 and timelineData['Items'][-1]['group'] == player['id'] and \
                    timelineData['Items'][-1]['type'] == 'background' and timelineData['Items'][-1]['style'] == toolLevelColor:
                    timelineData['Items'][-1]['end'] = rc['et']
                else:
                    timelineData['Items'].append({'start': rc['st'], 'end': rc['et'], 'group': player['id'], 'subgroup': 'sg0', 
                                                'type': 'background', 'style': toolLevelColor})

        for tg in player['triaging']:
            if tg['triaged']:
                timelineData["Items"].append(
                    {
                        "start": tg["st"],
                        "end": tg["et"],
                        "title": "Triaging " + ("cv-" if tg["is-critical"] else "v-") + str(tg["vid"]),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "editable": False,
                        "className": "def_triaging-success",
                    }
                )
                timelineData["Items"].append(
                    {
                        "start": tg["et"],
                        "title": "Triaged " + ("cv-" if tg["is-critical"] else "v-") + str(tg["vid"]),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "type":"point",
                        "editable": False,
                        "className": (
                            "def_triaged-critical-victim" if tg["is-critical"] else "def_triaged-victim"
                        ),
                    }
                )   
            else:
                timelineData['Items'].append({'start': tg['st'], 'end': tg['et'], 
                                              'title': 'Triaging ' + ('cv-' if tg['is-critical'] else 'v-') + str(tg['vid']) + ' (will fail)', 
                                              'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order, 'editable': False, 'className': 'def_triaging-fail'})

        for fz in player['frozen']:
            timelineData['Items'].append({'start': fz['st'], 'end': fz['et'], 'title': 'Frozen', 
                                          'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order, 'editable': False, 'className': 'def_frozen'})

        for ca in player['carrying']:
            timelineData['Items'].append({'start': ca['st'], 'end': ca['et'], 'title': 'Carrying ' + ('cv-' if ca['is-critical'] else 'v-')  + str(ca['vid']), 
                                          'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order, 'editable': False, 'className': 'def_carrying-victim'})

        for rb in player['rubble']:
            timelineData['Items'].append({'start': rb['st'], 'title': 'Rubble Cleared', 'group': player['id'], 'subgroup': 'sg0', 'subgroupOrder': sg0_order, 
                                          'type': 'point', 'editable': False, 'className': 'def_rubble', 'rubble_id': rb['rid']})

        # ts = 0
        # for diId in player['jag']:
        #     disc = player['jag'][diId]
        #     victim_id = disc["inputs"]["victim-id"] if "inputs" in disc and "victim-id" in disc["inputs"] else None
        #     victim_type = disc["inputs"]["victim-type"] if "inputs" in disc and "victim-type" in disc["inputs"] else None

        #     if victim_id is None or victim_type is None:
        #         continue
            
        #     if victim_id == 19:
        #         print('"player": "' + player['callsign'] + '",')
        #         print('"participant_id": "' + player['participant_id'] + '",')
        #         print(json.dumps(disc))

        #     # disc_leaves = getChildLeaves(disc)
        #     # for leaf in disc_leaves:
        #     #     if victim_id == 19:
        #     #         print(json.dumps(leaf) + ",")

        #     #     # if 'jags' not in leaf:
        #     #     #     continue
        #     #     # for jag in leaf['jags']:
        #     #     #     awareness = player['jag']['Event:Awareness'][leaf['id']] if 'Event:Awareness' in player['jag'] and leaf['id'] in player['jag']['Event:Awareness'] else None
        #     #     #     preparing = player['jag']['Event:Preparing'][leaf['id']] if 'Event:Preparing' in player['jag'] and leaf['id'] in player['jag']['Event:Preparing'] else None
        #     #     #     addressing = player['jag']['Event:Addressing'][leaf['id']] if 'Event:Addressing' in player['jag'] and leaf['id'] in player['jag']['Event:Addressing'] else None
        #     #     #     completion = player['jag']['Event:Completion'][leaf['id']] if 'Event:Completion' in player['jag'] and leaf['id'] in player['jag']['Event:Completion'] else None

        #     #     #     st = 999999999
        #     #     #     et = 0
        #     #     #     if awareness is not None:
        #     #     #         if awareness[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = awareness[-1]['elapsed_milliseconds']
        #     #     #         if awareness[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = awareness[-1]['elapsed_milliseconds']
        #     #     #     if preparing is not None:
        #     #     #         if preparing[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = preparing[-1]['elapsed_milliseconds']
        #     #     #         if preparing[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = preparing[-1]['elapsed_milliseconds']
        #     #     #     if addressing is not None:
        #     #     #         if addressing[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = addressing[-1]['elapsed_milliseconds']
        #     #     #         if addressing[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = addressing[-1]['elapsed_milliseconds']
        #     #     #     if completion is not None:
        #     #     #         if completion[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = completion[-1]['elapsed_milliseconds']
        #     #     #         if completion[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = completion[-1]['elapsed_milliseconds']

        #     #     #     timelineData['Items'].append({'start': ts, 'end': et, 'group': player['id'], 'subgroup': 'sg1', 'subgroupOrder': sg1_order,
        #     #     #                                     'editable': False, 'className': 'def_carrying-victim',
        #     #     #                                     'title': leaf["urn"].split(":")[-1] + " " + str(victim_id) + " " + victim_type[0]})

        # print()

        playerColor = 'background-color: ' + player['callsign'] + ';'
        timelineData['Items'].append({'start': 0, 'end': 20*60000, 'group': player['id'], 'subgroup': 'sgc', 'subgroupOrder': sgc_order,
            'type': 'background', 'style': playerColor})


    return timelineData

def processJag(jag, jagGroups, callsign, pid, planning_stop_time):
    if jag.has_children:
        for child in jag.children:
            processJag(child, jagGroups, callsign, pid, planning_stop_time)
    else:
        activity_type = jag.urn.split(":")[-1]
        title = activity_type + ' - ' + str(jag.inputs['victim-id']) + ' ' + jag.inputs['victim-type'][0]

        short_title = str(jag.inputs['victim-id']) + jag.inputs['victim-type'][0] + ' ' + activity_type[0:3].upper()

        summary = " - " + title
        handled = False

        prep_list = None
        addr_list = None

        preparing = jag.get_preparing()
        if callsign in preparing:
            prep_list = preparing[callsign]
        addressing = jag.get_addressing()
        if callsign in addressing:
            addr_list = addressing[callsign]

        size = len(prep_list) if prep_list is not None else 0
        # print("There are " + str(size) + " prepare activities for " + title)
        for i in range(0, size):
            jag_activity = {'start': None, 'end': None, 'items':[], 'main': None}

            activity = prep_list[i]
            time_period = activity.time_period
            if time_period.has_ended:
                ts = time_period.start if time_period.start >= planning_stop_time else planning_stop_time
                te = time_period.end if time_period.end >= planning_stop_time else planning_stop_time
                jag_activity['start'] = ts
                jag_activity['end'] = te
                if ts == te:
                    te = ts + 100
                jag_activity['items'].append({'start': ts, 'end': te, 'group': pid, 'subgroup': 'sg2', 'subgroupOrder': sg2_order,
                                              'editable': False, 'className': 'def_jag_preparing', 'style': 'background-color: rgba(100, 100, 100, 0.5); height: 10px;',
                                              'title': title + " preparing"})

                summary += "    - preparing: " + str(ts) + " - " + str(te)
                handled = True

            if addr_list is not None and i < len(addr_list):
                activity = addr_list[i]
                time_period = activity.time_period
                if time_period.has_ended:
                    ts = time_period.start if time_period.start >= planning_stop_time else planning_stop_time
                    te = time_period.end if time_period.end >= planning_stop_time else planning_stop_time
                    if jag_activity['start'] is None or jag_activity['start'] > ts:
                        jag_activity['start'] = ts
                    if jag_activity['end'] is None or jag_activity['end'] < te:
                        jag_activity['end'] = te
                    if ts == te:
                        te = ts + 100
                    jag_activity['items'].append({'start': ts, 'end': te, 'group': pid, 'subgroup': 'sg2', 'subgroupOrder': sg2_order,
                                                    'editable': False, 'className': 'def_jag_addressing', 'style': 'background-color: rgba(200, 200, 200, 0.5); height: 15px;',
                                                    'title': title + " addressing"})

                    summary += "    - addressing: " + str(ts) + " - " + str(te)
                    handled = True

            if jag_activity['start'] is not None and jag_activity['end'] is not None:
                style = activity_style_map[activity_type] if activity_type in activity_style_map else activity_style_map['default']
                jag_activity['main'] = {'start': jag_activity['start'], 'end': jag_activity['end'], 'group': pid, 'subgroup': 'sg2', 'subgroupOrder': sg2_order, 
                                    'editable': False, 'className': 'def_jag_activity', 'style': style,
                                    'content': short_title, 'title': title}
                updateJagGroups(jag_activity, jagGroups)

        if handled:
            print(summary)

def updateJagGroups(jag_activity, jagGroups):
    if jag_activity is None or 'main' not in jag_activity or jag_activity['main'] is None:
        return

    for jg in jagGroups:
        overlayed = False
        for ja in jg:
            if 'main' in ja and ja['main'] is not None:
                start = max(ja['main']['start'], jag_activity['main']['start'])
                end = min(ja['main']['end'], jag_activity['main']['end'])
                if start < end:
                    overlayed = True
                    break
        if not overlayed:
            jg.append(jag_activity)
            return
    
    jagGroups.append([jag_activity])
